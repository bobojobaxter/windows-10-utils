﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Threading;
using System.DirectoryServices;
using System.Text.RegularExpressions;
using System.DirectoryServices.AccountManagement;
using System.IO;
using WindowsFormsApp1.Properties;
using CredentialManagement;
using DialogResult = System.Windows.Forms.DialogResult;

namespace WindowsFormsApp1
{
	public partial class Win10Utils : Form
	{
		public Win10Utils()
		{
			InitializeComponent();
			var cm = new Credential();
			cm.Target = "Win10Utils";
			cm.Load();
			adminuser = cm.Username;
			adminpass = cm.Password;
			username.Text = "lw\\" + adminuser;
			password.Text = adminpass;
			this.Text = this.Text + " - " + adminuser;
			tabs.TabPages.Clear();
			tabs.TabPages.Add(tabPage1);

		}
		string startTime = DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss");
		public static string adminuser = loginWindow.user;
		public static string adminpass = loginWindow.pass;
		public static string copyUser = "";
		public static string addUser = "";
		string cPath = @"c:\ProgramData\Lambweston\GroupMembershipTool\ComputerInfo.log";
		string uPath = @"c:\ProgramData\Lambweston\GroupMembershipTool\UserInfo.log";
		int numlines = 0;

		/*

        #####################################################
        ##
        ##
        ##   User Lookup Functions
        ##
        ##
        ######################################################

        */

		private static IEnumerable<string> GetGroupsFindByIdentity(string username)
		{
			var results = new List<string>();
			using (var context = new PrincipalContext(ContextType.Domain))
			{
				try
				{
					UserPrincipal p = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, username);
					if (p != null)
					{
						var groups = p.GetGroups();
						foreach (var group in groups)
						{
							try
							{
								results.Add(@group.Name);
							}
							catch (Exception ex)
							{
								Debug.WriteLine(ex);
							}
						}
					}
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex);

				}
			}

			return results;
		}
		private List<string> adUser(string username)
		{
			var adresults = new List<string>();

			PrincipalContext ctx = new PrincipalContext(ContextType.Domain);
			UserPrincipal qbeUser = new UserPrincipal(ctx);


			qbeUser.DisplayName = username + "*";

			// create your principal searcher passing in the QBE principal    
			PrincipalSearcher srch = new PrincipalSearcher(qbeUser);

			// find all matches
			foreach (var found in srch.FindAll())
			{
				// do whatever here - "found" is of type "Principal" - it could be user, group, computer.....  
				string adName = found.SamAccountName.ToString() + " - " + found.DisplayName.ToString();
				adresults.Add(adName);
			}
			return adresults;
		}
		public void AddUserToGroup(string userId, List<string> groupName)
		{
			try
			{
				using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, "lambweston.com", adminuser, adminpass))
				{
					user_log.AppendText("Adding groups to " + userId + ":");
					user_log.AppendText(Environment.NewLine);
					foreach (string group in groupName)
					{
						GroupPrincipal groupadd = GroupPrincipal.FindByIdentity(pc, group);
						try
						{
							groupadd.Members.Add(pc, IdentityType.SamAccountName, userId);
							user_log.AppendText(group);
							user_log.AppendText(Environment.NewLine);
							groupadd.Save();
						}
						catch (Exception ex)
						{
							user_log.AppendText("Operation failed: " + ex.Message);
							user_log.AppendText(Environment.NewLine);
						}
					}
				}
			}
			catch (System.DirectoryServices.DirectoryServicesCOMException E)
			{
				//doSomething with E.Message.ToString(); 
				Debug.WriteLine(E.Message.ToString());
				MessageBox.Show("There was an error processing this request. The error reported: " + E.Message.ToString());
			}
		}
		public void RemoveUserFromGroup(string userId, List<string> groupName)
		{
			try
			{
				using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, "lambweston.com", adminuser, adminpass))
				{
					user_log.AppendText("Removing groups from " + userId + ":");
					user_log.AppendText(Environment.NewLine);
					foreach (string group in groupName)
					{
						GroupPrincipal groupdel = GroupPrincipal.FindByIdentity(pc, group);
						try
						{
							groupdel.Members.Remove(pc, IdentityType.SamAccountName, userId);
							user_log.AppendText(group);
							user_log.AppendText(Environment.NewLine);
							groupdel.Save();
						}
						catch (Exception ex)
						{
							user_log.AppendText("Operation failed: " + ex.Message);
							user_log.AppendText(Environment.NewLine);
						}
					}
				}
			}
			catch (System.DirectoryServices.DirectoryServicesCOMException E)
			{
				//doSomething with E.Message.ToString(); 
				Debug.WriteLine(E.Message.ToString());
				MessageBox.Show("There was an error processing this request. The error reported: " + E.Message.ToString());
			}
		}
		private void LookupUsers()
		{
			comboBox1.Items.Clear();
			comboBox2.Items.Clear();

			if (string.IsNullOrEmpty(User1.Text))
			{
				//MessageBox.Show("Nothing entered in user to copy.");
				user_log.AppendText("Nothing was entered in the user to copy.");
				user_log.AppendText(Environment.NewLine);
			}
			else
			{

				List<string> adUserList = adUser(User1.Text);
				if (adUserList.Count == 0)
				{

				}
				else
				{
					adUserList.Sort();
					comboBox1.Enabled = true;
					user_log.AppendText("Found users:");
					user_log.AppendText(Environment.NewLine);
					foreach (string user in adUserList)
					{

						comboBox1.Items.Add(user);
						user_log.AppendText(user);
						user_log.AppendText(Environment.NewLine);
					}
				}
			}
			if (string.IsNullOrEmpty(user2.Text))
			{
				//MessageBox.Show("Nothing entered in user to add.");
				user_log.AppendText("Nothing was entered in the user to add.");
				user_log.AppendText(Environment.NewLine);
			}
			else
			{

				List<string> adUserList2 = adUser(user2.Text);
				if (adUserList2.Count == 0)
				{

				}
				else
				{
					adUserList2.Sort();
					comboBox2.Enabled = true;
					user_log.AppendText("Found users:");
					user_log.AppendText(Environment.NewLine);
					foreach (string user in adUserList2)
					{
						comboBox2.Items.Add(user);
						user_log.AppendText(user);
						user_log.AppendText(Environment.NewLine);
					}
				}
			}
		}

		private void lookup_user_to_copy_Click(object sender, EventArgs e)
		{
			copy_user_groups.Items.Clear();
			add_user_groups_button.Enabled = false;
			string s = comboBox1.SelectedItem.ToString();
			string split = s.Substring(0, s.IndexOf(' '));
			copyUser = split;
			var groups = GetGroupsFindByIdentity(split);
			List<string> grouplist = groups.ToList<string>();

			if (groups != null)
			{
				copy_user_groups.Enabled = true;
				user_log.AppendText("Found groups for " + s);
				user_log.AppendText(Environment.NewLine);
				foreach (string group in grouplist)
				{
					copy_user_groups.Items.Add(group);
					user_log.AppendText(group);
					user_log.AppendText(Environment.NewLine);
				}
			}
			else
			{
				copy_user_groups.Items.Add("No groups found...check user name...");
			}
		}
		private void lookup_user_to_add_Click(object sender, EventArgs e)
		{
			add_user_groups.Items.Clear();
			string s = comboBox2.SelectedItem.ToString();
			string split = s.Substring(0, s.IndexOf(' '));
			addUser = split;
			var groups = GetGroupsFindByIdentity(split);
			List<string> grouplist = groups.ToList<string>();
			if (groups != null)
			{
				copy_user_groups.Enabled = true;
				user_log.AppendText("Found groups for " + s);
				user_log.AppendText(Environment.NewLine);
				foreach (string group in grouplist)
				{
					add_user_groups.Items.Add(group);
					user_log.AppendText(group);
					user_log.AppendText(Environment.NewLine);
				}
			}
			else
			{
				add_user_groups.Items.Add("No groups found...check user name...");
				user_log.AppendText("No groups found, please check username...");
				user_log.AppendText(Environment.NewLine);
			}
		}
		/*
         #################################################
         ##
         ##  AD Computer Lookups
         ##
         #################################################
         */
		private string GetCurrentDomainPath()
		{
			DirectoryEntry de =
			   new DirectoryEntry("LDAP://RootDSE");

			return "LDAP://" +
			   de.Properties["defaultNamingContext"][0].
				   ToString();
		}


		// PING Function start
		public string Pinger(string hostname)
		{

			string netstatus = "empty";
			Ping ping = new Ping();
			try
			{
				string formatHost = hostname.ToUpper();
				Debug.WriteLine("Pinging " + formatHost + ".....");
				log.AppendText("Pinging " + formatHost + ".....");
				PingReply reply = ping.Send(hostname, 4000);
				if (reply.Status == IPStatus.Success)
				{

					Debug.WriteLine(hostname + " is online...");
					log.AppendText("online!");
					log.AppendText(Environment.NewLine);
					netstatus = "Online";
				}
				else
				{
					Debug.WriteLine(hostname + " is offline...");
					log.AppendText(hostname + " is offline.");
					log.AppendText(Environment.NewLine);
					netstatus = "Offline";
				}
			}
			catch
			{
				Debug.WriteLine("Can't find host " + hostname + "...");
				log.AppendText("Can't find DNS Host, either bad hostname or offline for a long time...");
				log.AppendText(Environment.NewLine);
				netstatus = "No DNS Host";
			}
			return netstatus;
		}
		// Lookup AD Computer CN
		public string ComputerDN(string hostname)
		{
			try
			{
				string memberDN;
				DirectorySearcher dom = new DirectorySearcher(GetCurrentDomainPath());
				dom.Filter = "(&(ObjectCategory=Computer) (cn=" + hostname + "))";
				SearchResult searchResult = dom.FindOne();
				memberDN = searchResult.GetDirectoryEntry().Path;
				Debug.WriteLine("CN of new computer is: " + memberDN);
				return memberDN;
			}
			catch (Exception ex)
			{
				return ex.Message;
			}
		}
		// Lookup AD User CN

		//Lookup group DN
		public string GroupDN(string group)
		{
			string groupDN = null;
			DirectorySearcher group_search = new DirectorySearcher(GetCurrentDomainPath());
			group_search.Filter = "(&(ObjectCategory=Group) (cn=" + group + "))";
			SearchResult gsr = group_search.FindOne();
			groupDN = gsr.GetDirectoryEntry().Path;
			return groupDN;
		}
		// AD Description lookup
		public string ComputerObject(string hostname)
		{
			log.AppendText("Looking up compuper description for " + hostname + "...");
			log.AppendText(Environment.NewLine);
			string description = "<Name of User> - <PC Type/Function>";
			try
			{
				DirectorySearcher ds = null;
				DirectoryEntry de = new
				   DirectoryEntry(GetCurrentDomainPath());
				ds = new DirectorySearcher(de);
				ds.Filter = "(cn=" + hostname + ")";
				ds.PropertiesToLoad.Add("Description");
				SearchResult result = ds.FindOne();
				if (result == null)
				{

				}
				else
				{
					description = result.Properties["description"][0].ToString();
				}
			}
			catch (Exception ex)
			{
				log.AppendText("Encountered Error looking up description..." + ex.Message);
			}

			return description;
		}

		// AD Group Search Function
		public List<string> ADQ(string hostname)
		{
			var adresults = new List<string>();
			DirectorySearcher ds = null;
			DirectoryEntry de = new
			   DirectoryEntry(GetCurrentDomainPath());
			ds = new DirectorySearcher(de);
			ds.Filter = "(cn=" + hostname + ")";
			//ds.Filter = "(" + hostname + ")";
			ds.PropertiesToLoad.Add("memberOf");
			var result = ds.FindAll();
			if (result != null)
			{
				Debug.WriteLine("Got AD Results!");
				foreach (SearchResult sr in result)
				{
					if (sr.Properties["memberof"].Count > 0)
					{
						Debug.WriteLine("Member of...");
						log.AppendText("Member of...");
						log.AppendText(Environment.NewLine);
						foreach (string item in sr.Properties["memberof"])
						{
							Debug.WriteLine(item);
							log.AppendText(item);
							log.AppendText(Environment.NewLine);
							string cnPattern = @"^CN=(?<cn>.+?)(?<!\\),";
							string dn = item;
							Regex re = new Regex(cnPattern);
							Match m = re.Match(dn);
							if (m.Success)
							{
								// Item with index 1 returns the first group match.
								string cn = m.Groups[1].Value;
								adresults.Add(cn);
							}
						}
					}
				}
			}
			else
			{
				Debug.WriteLine("This didn't work.");
			}
			return adresults;
		}
		// AD Add computer group function start
		public void ADAdd(string hostname, List<string> groups)
		{
			Debug.WriteLine("Adding groups to " + hostname);
			try
			{
				string memberDN = ComputerDN(hostname);
				foreach (string item in groups)
				{
					log.AppendText("Adding group: " + item + " to " + hostname + "...");
					if (item.Contains("DirectAccess") || item.Contains("anyconnect"))
					{
						log.AppendText("Not adding Direct Access/Anyconnect Group!");
					}
					else
					{
						log.AppendText(Environment.NewLine);
						String groupDN = GroupDN(item);
						DirectoryEntry groupEntry = new DirectoryEntry(groupDN);
						groupEntry.Username = username.Text;
						groupEntry.Password = password.Text;
						groupEntry.Invoke("Add", memberDN);
						new_computer_groups.Items.Add(item);
					}
				}
			}
			catch (Exception ex)
			{
				log.AppendText("Unable to add groups " + hostname + ". Error: " + ex);
				log.AppendText(Environment.NewLine);
			}
		}
		// AD Remove computer groups function start
		public void ADDel(string hostname, List<string> groups)
		{
			Debug.WriteLine("Removing groups from " + hostname);
			try
			{
				string memberDN = ComputerDN(hostname);
				foreach (string item in groups)
				{
					log.AppendText("Removing group " + item + " from " + hostname + "...");
					log.AppendText(Environment.NewLine);
					String groupDN = GroupDN(item);
					DirectoryEntry groupEntry = new DirectoryEntry(groupDN);
					groupEntry.Username = username.Text;
					groupEntry.Password = password.Text;
					groupEntry.Invoke("Remove", memberDN);
					new_computer_groups.Items.Remove(item);
				}
			}
			catch (Exception ex)
			{
				log.AppendText("Unable to remove groups from " + hostname + "..." + ex);
				log.AppendText(Environment.NewLine);
			}
		}
		private void Win10Utils_Load(object sender, EventArgs e)
		{



		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			//WinUtils_FormClosing(null, null);
			this.Close();
		}
		private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Properties.AboutBox box = new AboutBox();
			box.ShowDialog();
		}
		private void new_computer_TextChanged(object sender, EventArgs e)
		{
			new_desc.Text = "";
			new_desc.Enabled = false;
			move_ou_button.Enabled = true;
		}
		private void computer_description_TextChanged(object sender, EventArgs e)
		{
			set_desc.Enabled = true;
		}
		private void lookup_groups_button_Click(object sender, EventArgs e)
		{
			groups_listbox.Items.Clear();
			new_computer_groups.Items.Clear();


			if (string.IsNullOrEmpty(old_computer.Text))
			{
				Debug.WriteLine("No hostname entered in old system field...");
				log.AppendText("No hostname in old system field...");
				log.AppendText(Environment.NewLine);
			}
			else
			{

				string status = Pinger(old_computer.Text);
				system_status.Visible = true;
				if (status == "Online")
				{
					system_status.Enabled = true;
					system_status.ForeColor = Color.Green;
				}
				else
				{
					system_status.Enabled = false;
				}
				system_status.Text = status;
				List<string> adSearch = ADQ(old_computer.Text);
				foreach (string group in adSearch)
				{
					groups_listbox.Items.Add(group);
				}
				string desc = ComputerObject(old_computer.Text);
				Debug.WriteLine("Checkpoint 1");
				if (desc.Contains("Type"))
				{
					computer_description.Text = "<Name of User> - <PC Type/Function>";
					computer_description.ForeColor = Color.Red;
					Debug.WriteLine("Checkpoint 2");
				}
				else
				{
					computer_description.Text = desc;
					computer_description.ForeColor = Color.Blue;
					set_desc.Enabled = true;
					computer_description.Enabled = true;
					Debug.WriteLine("Checkpoint 3");
				}
			}

			if (string.IsNullOrEmpty(new_computer.Text))
			{
				Debug.WriteLine("No hostname entered in new system field...");
				log.AppendText("No hostname in old system field...");
				log.AppendText(Environment.NewLine);
			}
			else
			{
				string new_descl = ComputerObject(new_computer.Text);
				new_desc.Text = new_descl;
				new_desc.Enabled = true;
				string status2 = Pinger(new_computer.Text);
				new_system_status.Visible = true;
				if (status2 == "Online")
				{
					new_system_status.Enabled = true;
					new_system_status.ForeColor = Color.Green;
				}
				else
				{
					new_system_status.Enabled = false;
				}
				new_system_status.Text = status2;
				List<string> adSearch2 = ADQ(new_computer.Text);
				foreach (string group in adSearch2)
				{
					new_computer_groups.Items.Add(group);
				}
			}
		}
		private void old_computer_TextChanged(object sender, EventArgs e)
		{
			groups_listbox.Items.Clear();
		}

		private void sync_groups_Click(object sender, EventArgs e)
		{
			List<string> groups = new List<string>();
			foreach (string item in groups_listbox.SelectedItems)
			{
				groups.Add(item.ToString());
				Debug.WriteLine("Adding " + item + " to array 'groups'...");
			}
			ADAdd(new_computer.Text, groups);
			new_computer_groups.Enabled = true;

		}

		private void resetFormToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ClearTextBoxes(this);
			username.Text = "lw\\" + adminuser;
			password.Text = adminpass;
			new_system_status.Visible = false;
			computer_description.Text = "<Name of User> - <PC Type/Function>";
			computer_description.Enabled = false;
			new_desc.Enabled = false;
			copy_desc.Enabled = false;
			remove_right.Enabled = false;
			sync_groups.Enabled = false;
		}

		private void new_computer_label_Click(object sender, EventArgs e)
		{

		}

		private void groupBox1_Enter(object sender, EventArgs e)
		{

		}

		private void installedSoftware_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
		{

		}

		private void remove_right_Click(object sender, EventArgs e)
		{

			List<string> groups = new List<string>();
			foreach (string item in new_computer_groups.SelectedItems)
			{
				groups.Add(item.ToString());
				Debug.WriteLine("Adding " + item + " to array 'groups' to remove them from the ad container...");
			}
			ADDel(new_computer.Text, groups);
		}

		private void textBox1_TextChanged(object sender, EventArgs e)
		{

		}

		private void set_desc_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(new_computer.Text))
			{
				// Do nothing
			}
			else
			{
				string desc = new_desc.Text;
				Debug.WriteLine(desc);
				if (desc.Contains("Verify") == true)
				{
					MessageBox.Show("You need to update the description.");
				}
				else if (desc.Contains("Name of user") == true)
				{
					MessageBox.Show("You need to update the description.");
				}
				{
					try
					{
						string memberDN = ComputerDN(new_computer.Text);
						DirectoryEntry description = new DirectoryEntry(memberDN);
						description.Username = username.Text;
						description.Password = password.Text;
						description.Properties["description"].Value = desc;
						Debug.WriteLine("Updating computer description to show: " + desc);
						description.CommitChanges();
						log.AppendText("Updating computer descriotion for " + new_computer.Text + " to:");
						log.AppendText(Environment.NewLine);
						log.AppendText(desc);
					}
					catch (Exception ex)
					{
						Debug.WriteLine("This failed with exception: " + ex);
					}

				}
			}
		}

		private void pingReportToolStripMenuItem_Click(object sender, EventArgs e)
		{
			bulk form2 = new bulk();
			{
				form2.ShowDialog();
			}
		}

		private void label4_Click(object sender, EventArgs e)
		{

		}

		private void copy_desc_Click(object sender, EventArgs e)
		{
			new_desc.Text = computer_description.Text;
		}

		private void log_TextChanged(object sender, EventArgs e)
		{

		}
		private void username_TextChanged(object sender, EventArgs e)
		{
			username1.Text = username.Text;
		}
		private void password_TextChanged(object sender, EventArgs e)
		{
			password1.Text = password.Text;
		}
		private void button1_Click(object sender, EventArgs e)
		{
			LookupUsers();
		}
		private void User1_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
			{
				LookupUsers();

			}
		}
		private void copy_user_groups_SelectedValueChanged(object sender, EventArgs e)
		{
			add_user_groups_button.Enabled = true;
		}
		private void add_user_groups_button_Click(object sender, EventArgs e)
		{
			user_log.AppendText("Adding groups to user " + addUser + ":");
			user_log.AppendText(Environment.NewLine);
			List<string> groups_to_add = new List<string>();
			foreach (object item in copy_user_groups.SelectedItems)
			{
				groups_to_add.Add(item.ToString());
				user_log.AppendText(item.ToString());
				user_log.AppendText(Environment.NewLine);
			}
			AddUserToGroup(addUser, groups_to_add);
			copy_user_groups.ClearSelected();
		}
		private void add_user_groups_SelectedIndexChanged(object sender, EventArgs e)
		{
			remove_from_right_button.Enabled = true;

		}

		private void remove_from_right_button_Click(object sender, EventArgs e)
		{
			user_log.AppendText("Removing groups from user " + addUser + ":");
			user_log.AppendText(Environment.NewLine);
			List<string> groups_to_remove = new List<string>();
			foreach (object item in add_user_groups.SelectedItems)
			{
				groups_to_remove.Add(item.ToString());
				RemoveUserFromGroup(addUser, groups_to_remove);

			}
			lookup_user_to_add_Click(null, null);
			remove_from_right_button.Enabled = false;
			add_user_groups.ClearSelected();
		}

		private void remove_from_left_button_Click(object sender, EventArgs e)
		{
			user_log.AppendText("Removing groups from user " + copyUser + ":");
			user_log.AppendText(Environment.NewLine);
			List<string> groups_to_remove = new List<string>();
			foreach (object item in copy_user_groups.SelectedItems)
			{
				groups_to_remove.Add(item.ToString());
				RemoveUserFromGroup(copyUser, groups_to_remove);
			}
		}

		private void User1_TextChanged(object sender, EventArgs e)
		{
			comboBox1.Items.Clear();
			comboBox1.Text = "Select User...";
			comboBox1.Enabled = false;
		}

		private void user2_TextChanged(object sender, EventArgs e)
		{
			comboBox2.Items.Clear();
			comboBox2.Text = "Select User...";
			comboBox2.Enabled = false;
		}

		public class Utilities
		{
			public static void ResetAllControls(Control form)
			{
				foreach (Control control in form.Controls)
				{

					if (control is TextBox)
					{
						TextBox textBox = (TextBox)control;
						textBox.Text = "";
					}

					if (control is ComboBox)
					{
						ComboBox comboBox = (ComboBox)control;
						if (comboBox.Items.Count > 0)
							comboBox.SelectedIndex = 0;
						comboBox.Items.Clear();
					}

					if (control is CheckBox)
					{
						CheckBox checkBox = (CheckBox)control;
						checkBox.Checked = false;
					}

					if (control is ListBox)
					{
						ListBox listBox = (ListBox)control;
						listBox.ClearSelected();
						listBox.Items.Clear();

					}
				}
			}
		}
		protected override void OnFormClosing(FormClosingEventArgs e)
		{
			base.OnFormClosing(e);
			// your code...

			string endTime = DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss");
			try
			{
				StreamWriter cl = new StreamWriter(cPath, append: true);
				if (log.Text != null)
				{
					cl.WriteLine(startTime + " - Application Launched");
					cl.WriteLine(log.Text);
					cl.WriteLine(endTime + " - Application Exited");
					cl.Close();
				}
				if (user_log.Text != null)
				{
					StreamWriter ul = new StreamWriter(uPath, append: true);
					ul.WriteLine(startTime + " - Application Launched");
					ul.WriteLine(user_log.Text);
					ul.WriteLine(endTime + " - Application Exited");
					ul.Close();
				}
			}
			catch
			{

			}
		}

		private void groups_listbox_SelectedIndexChanged(object sender, EventArgs e)
		{
			sync_groups.Enabled = true;
		}

		private void new_computer_groups_SelectedIndexChanged(object sender, EventArgs e)
		{
			remove_right.Enabled = true;

		}

		private void new_computer_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
			{
				lookup_groups_button_Click(null, null);
			}
		}

		private void computerGroupsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			tabs.TabPages.Clear();
			tabs.TabPages.Add(tabPage1);
		}

		private void userGroupsToolStripMenuItem_Click(object sender, EventArgs e)
		{

			tabs.TabPages.Clear();
			tabs.TabPages.Add(tabPage2);
		}
		private void pingReportToolStripMenuItem1_Click(object sender, EventArgs e)
		{
			tabs.TabPages.Clear();
			tabs.TabPages.Add(tabPage3);
		}

		private void profileMigrationToolStripMenuItem_Click(object sender, EventArgs e)
		{
			tabs.TabPages.Clear();
			tabs.TabPages.Add(migration);
			string pspath = Path.Combine(Path.GetTempPath(), "psexec.exe");
			Debug.WriteLine(pspath);
			File.WriteAllBytes(pspath, WindowsFormsApp1.Properties.Resources.PsExec);
		}


		private void button2_Click(object sender, EventArgs e)
		{
			lookup_user_to_add_Click(null, null);
			button2.TabStop = false;

		}

		private void button3_Click(object sender, EventArgs e)
		{
			lookup_user_to_copy_Click(null, null);
			button1.TabStop = false;
		}
		private void computer_list_TextChanged(object sender, EventArgs e)
		{
			numlines = computer_list.Lines.Count();
			computer_list.CharacterCasing = CharacterCasing.Upper;
		}





		/*
         * 
         *   Check accidental Closing of the application.
         * 
         * 
         * 
        */
		private bool m_isExiting;
		private void WinUtils_FormClosing(object sender, FormClosingEventArgs e)
		{

			if (!m_isExiting)
			{
				DialogResult Response;
				Response = MessageBox.Show("Are you sure you want to Exit?", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
				if (Response == DialogResult.Yes)
				{
					m_isExiting = true;
					Application.Exit();
				}
				else if (Response == DialogResult.No)
				{

					e.Cancel = true;

				}
			}
		}

		private void button4_Click(object sender, EventArgs e)
		{
			Stream myStream = null;
			OpenFileDialog openFileDialog1 = new OpenFileDialog();

			openFileDialog1.InitialDirectory = "%userprofile%";
			openFileDialog1.Filter = "txt files (*.txt)|*.txt";
			openFileDialog1.FilterIndex = 2;
			openFileDialog1.RestoreDirectory = true;

			if (openFileDialog1.ShowDialog() == DialogResult.OK)
			{
				try
				{
					if ((myStream = openFileDialog1.OpenFile()) != null)
					{
						using (myStream)
						{
							// Insert code to read the stream here.
						}
					}
				}
				catch (Exception ex)
				{
					MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
				}
				computer_list.Text = File.ReadAllText(openFileDialog1.FileName);
				string trimmed = computer_list.Text.ToString();
				computer_list.Text = trimmed.Trim();
				myStream.Close();
			}
		}

		private void button5_Click(object sender, EventArgs e)
		{
			string trimmed = computer_list.Text.ToString();
			computer_list.Text = trimmed.Trim();
			int line = 1;
			ping_report.Text = "";
			while (line <= numlines)
			{
				string netstatus = Pinger(computer_list.Lines[line - 1]);
				//List<string> group_list = ADQ(computer_list.Lines[line - 1]);
				//groups.Items.Add(computers.Lines[line - 1] + "----" + netstatus.ToUpper());
				ping_report.AppendText(computer_list.Lines[line - 1] + "," + netstatus.ToUpper());
				if (include_groups_checkbox.Checked == true)
				{
					List<string> group_list = ADQ(computer_list.Lines[line - 1]);
					foreach (string sr in group_list)
					{
						ping_report.AppendText("," + sr);
					}
				}
				ping_report.AppendText(Environment.NewLine);
				line++;
			}
			FormatTextBox(ping_report, "ONLINE", Color.Gray, Color.Green);
		}
		private void FormatTextBox(RichTextBox richText, string p,
			Color textColor, Color highColor)
		{
			string[] lines = richText.Lines;
			richText.Text = "";

			foreach (string line in lines)
			{
				richText.SelectionColor = line.Contains(p) ? highColor : textColor;
				richText.AppendText(line + "\n");
			}
		}

		private void button7_Click(object sender, EventArgs e)
		{
			migration_log.AppendText("Pasting data from Excel into host fields...");
			migration_log.AppendText(Environment.NewLine);
			string savelog = migration_log.Text;
			ClearTextBoxes(this);
			migration_log.Text = savelog;
			// COPY DATA FROM CLIPBOARD
			string excelList = null;
			if (Clipboard.ContainsText(TextDataFormat.Text))
			{
				List<string> excelData = new List<string>();

				excelList = Clipboard.GetText(TextDataFormat.Text);
				string[] formatList = excelList.Split(new[] { '\t', '\n' }, StringSplitOptions.RemoveEmptyEntries);

				try
				{
					old01.Text = formatList[0].Trim();
					new01.Text = formatList[1].Trim();
					old2.Text = formatList[2].Trim();
					new2.Text = formatList[3].Trim();
					old3.Text = formatList[4].Trim();
					new3.Text = formatList[5].Trim();
					old4.Text = formatList[6].Trim();
					new4.Text = formatList[7].Trim();
					old5.Text = formatList[8].Trim();
					new5.Text = formatList[9].Trim();
					old6.Text = formatList[1].Trim();
					new6.Text = formatList[11].Trim();
					old7.Text = formatList[12].Trim();
					new7.Text = formatList[13].Trim();
					old8.Text = formatList[14].Trim();
					new8.Text = formatList[15].Trim();
					old9.Text = formatList[16].Trim();
					new9.Text = formatList[17].Trim();
					old10.Text = formatList[18].Trim();
					new10.Text = formatList[19].Trim();
				}
				catch
				{

				}
			}
		}
		private void ClearTextBoxes(Control control)
		{
			foreach (Control c in control.Controls)
			{
				if (c is RichTextBox)
				{
					if (!c.Name.Contains("log"))
					{
						((RichTextBox)c).Clear();
					}
				}
				if (c is TextBox)
				{
					if (!c.Name.Contains("log"))
					{
						((TextBox)c).Clear();
					}
				}

				if (c.HasChildren)
				{
					ClearTextBoxes(c);
				}


				if (c is CheckBox)
				{

					((CheckBox)c).Checked = false;
				}

				if (c is RadioButton)
				{
					((RadioButton)c).Checked = false;
				}
				if (c is ListBox)
				{
					((ListBox)c).Items.Clear();
				}
			}
		}

		private void copyserversbutton_Click(object sender, EventArgs e)
		{
			migration_log.AppendText("Setting all backup servers to be " + server01.Text);
			migration_log.AppendText(Environment.NewLine);
			foreach (Control c in server_jobs_groupbox.Controls)
			{
				if (c is ComboBox)
				{
					if (c.Name.Contains("server") && !c.Name.Contains("01"))
					{
						ComboBox comboBox = (ComboBox)c;
						comboBox.SelectedIndex = server01.SelectedIndex;
					}
				}
			}
		}

		private void clearserversbutton_Click(object sender, EventArgs e)
		{
			migration_log.AppendText("Clearing all servers....");
			migration_log.AppendText(Environment.NewLine);
			foreach (Control c in server_jobs_groupbox.Controls)
				if (c is ComboBox)
				{
					if (c.Name.Contains("server"))
					{
						ComboBox comboBox = (ComboBox)c;
						comboBox.SelectedIndex = -1;
					}
				}

		}

		private void button8_Click(object sender, EventArgs e)
		{
			List<string> formfields = new List<string>();
			List<string> jobactions = new List<string>();
			foreach (Control a in server_jobs_groupbox.Controls)
			{
				string fname = a.Name;
				formfields.Add(fname);
				ComboBox combo = a as ComboBox;
				if (combo != null)
				{
					// 0 = Backup and Load
					// 1 = Backup to Server only
					// 2 = Load existing from server only
					string cname = combo.Name;
					string jobNumber = Regex.Replace(cname, "[^0-9]", "");
					string jobtype = "";
					if (combo.SelectedIndex == 0 && fname.Contains("migration"))
					{
						jobtype = "BR";
						RunJobs(jobtype, jobNumber);
					}
					else if (combo.SelectedIndex == 1 && cname.Contains("migration"))
					{
						jobtype = "B";
						RunJobs(jobtype, jobNumber);
					}
					else if (combo.SelectedIndex == 2 && cname.Contains("migration"))
					{
						jobtype = "R";
						RunJobs(jobtype, jobNumber);

					}
				}
			}
			jobactions.Sort();
			formfields.Sort();
		}

		private void RunJobs(string type, string number)
		{
			string oldhost = "";
			string newhost = "";
			string servername = "";
			string jobstatus = "";
			string backupCMD1 = "";
			string loadCMD1 = "";

			Debug.WriteLine("Runjobs checkpoint 1.");
			if (type == "BR")
			{
				migration_log.AppendText("Starting job: " + number + " - Backup and Restore 1 step.");
				migration_log.AppendText(Environment.NewLine);
				foreach (Control c in server_jobs_groupbox.Controls)
				{
					if (c.Name.Contains(number) && c.Enabled == true)
					{
						try
						{
							if (c is TextBox)
							{
								if (((TextBox)c).Name.Contains("old"))
								{
									oldhost = ((TextBox)c).Text;
								}
								if (((TextBox)c).Name.Contains("new"))
								{
									newhost = ((TextBox)c).Text;
								}
							}
							if (c is ComboBox)
							{
								servername = ((ComboBox)c).Text;
							}
							if (c is Label)
							{
								((Label)c).Text = "Job Initiated";
								((Label)c).Visible = true;
								jobstatus = c.Name;
							}
							backupCMD1 = "\\\\" + oldhost + " -accepteula -e -h -u lw\\" + adminuser + " -p " + adminpass + " -w \"C:\\Temp\" \"C:\\Temp\\Win7Win10_BackupProfile.cmd\" " + servername;
							loadCMD1 = "\\\\" + newhost + " -accepteula -e -h -u lw\\" + adminuser + " -p " + adminpass + " -w \"C:\\Temp\" \"C:\\Temp\\Win10_LoadProfile.cmd\" " + servername + " " + oldhost;
						}
						catch (Exception ex)
						{
							migration_log.AppendText("Did not start job due to error: " + ex.Message);
							migration_log.AppendText(Environment.NewLine);
							Debug.WriteLine("Didn't capture host info properly because of error: " + ex.Message);
						}
					}
				}
				new Thread((ThreadStart)(() => this.RealStart(backupCMD1, loadCMD1, oldhost, newhost, jobstatus))).Start();
			}
			if (type == "B")
			{
				migration_log.AppendText("Starting job: " + number + " - Backup to server only.");
				migration_log.AppendText(Environment.NewLine);
				foreach (Control c in server_jobs_groupbox.Controls)
				{
					if (c.Name.Contains(number) && c.Enabled == true)
					{
						try
						{
							if (c is TextBox)
							{
								if (((TextBox)c).Name.Contains("old"))
								{
									oldhost = ((TextBox)c).Text;
								}
								if (((TextBox)c).Name.Contains("new"))
								{
									newhost = ((TextBox)c).Text;
								}
							}
							if (c is ComboBox)
							{
								servername = ((ComboBox)c).Text;
							}
							if (c is Label)
							{
								((Label)c).Text = "Job initiated";
								((Label)c).Visible = true;
								jobstatus = c.Name;
							}
							backupCMD1 = "\\\\" + oldhost + " -accepteula -e -h -u lw\\" + adminuser + " -p " + adminpass + " -w \"C:\\Temp\" \"C:\\Temp\\Win7Win10_BackupProfile.cmd\" " + servername;
						}
						catch (Exception ex)
						{
							migration_log.AppendText("Did not start job due to error: " + ex.Message);
							migration_log.AppendText(Environment.NewLine);
							Debug.WriteLine("Didn't capture host info properly because of error: " + ex.Message);
						}
					}
				}
				loadCMD1 = "";
				new Thread((ThreadStart)(() => this.RealStart(backupCMD1, null, oldhost, newhost, jobstatus))).Start();
			}
			if (type == "R")
			{
				migration_log.AppendText("Starting job: " + number + " - Restore data only.");
				migration_log.AppendText(Environment.NewLine);
				Debug.WriteLine("about to restore only");
				foreach (Control c in server_jobs_groupbox.Controls)
				{

					if (c.Name.Contains(number) && c.Enabled == true)
					{
						try
						{
							if (c is TextBox)
							{
								if (((TextBox)c).Name.Contains("old"))
								{
									oldhost = ((TextBox)c).Text;
								}
								if (((TextBox)c).Name.Contains("new"))
								{
									newhost = ((TextBox)c).Text;
								}
							}
							if (c is ComboBox)
							{
								servername = ((ComboBox)c).Text;
							}
							if (c is Label)
							{
								((Label)c).Text = "Job initiated";
								((Label)c).Visible = true;
								jobstatus = c.Name;
							}

							loadCMD1 = "\\\\" + newhost + " -accepteula -e -h -u lw\\" + adminuser + " -p " + adminpass + " -w \"C:\\Temp\" \"C:\\Temp\\Win10_LoadProfile.cmd\" " + servername + " " + oldhost;
						}
						catch (Exception ex)
						{
							migration_log.AppendText("Did not start job due to error: " + ex.Message);
							migration_log.AppendText(Environment.NewLine);
							Debug.WriteLine("Didn't capture host info properly because of error: " + ex.Message);
						}
					}
				}
				backupCMD1 = "";
				Debug.WriteLine("Made it to the restore command...not yet executed...line number 1367");
				new Thread((ThreadStart)(() => this.RealStart(backupCMD1, loadCMD1, oldhost, newhost, jobstatus))).Start();
			}
		}


		public void RealStart(string backup, string restore, string oldhost, string newhost, string jstatus)
		{
			if (string.IsNullOrEmpty(backup) && string.IsNullOrEmpty(restore))
			{
				UpdateLabel(jstatus, "");
			}
			if (!string.IsNullOrEmpty(backup) && !string.IsNullOrEmpty(restore))
			{
				Debug.WriteLine("made it here...");
				int timeout = 4000;
				Ping ping1 = new Ping();
				Ping ping2 = new Ping();
				try
				{
					ping1.Send(oldhost, timeout);
					ping2.Send(newhost, timeout);
					string backupcmdPath = @"\\" + oldhost + @"\c$\temp\Win7Win10_BackupProfile.cmd";
					string loadcmdPath = @"\\" + oldhost + @"\c$\temp\Win10_LoadProfile.cmd";

					File.WriteAllBytes(backupcmdPath, WindowsFormsApp1.Properties.Resources.backupcmd);
					File.WriteAllBytes(loadcmdPath, WindowsFormsApp1.Properties.Resources.loadcmd);
					string pspath = Path.Combine(Path.GetTempPath(), "psexec.exe");
					string backupjob = backup;
					UpdateLabel(jstatus, "Backup job started");
					Process executeJob = new Process()
					{
						StartInfo = {
						UseShellExecute = false,
						RedirectStandardOutput = true,
						RedirectStandardError = true,
						RedirectStandardInput = true,
						FileName = pspath.ToString(),
						Arguments = backupjob.ToString()
						}
					};
					executeJob.StartInfo.RedirectStandardOutput = true;
					executeJob.Start();
					executeJob.StandardOutput.ReadToEnd();
					executeJob.StandardError.ReadToEnd();
					UpdateLabel(jstatus, "Load Job Started");
					string restorejob = restore;
					Process executejob = new Process()
					{
						StartInfo = {
						UseShellExecute = false,
						RedirectStandardOutput = true,
						RedirectStandardError = true,
						RedirectStandardInput = true,
						FileName = pspath.ToString(),
						Arguments = restorejob.ToString()
						}
					};
					executejob.StartInfo.RedirectStandardOutput = true;
					executejob.Start();
					executejob.StandardOutput.ReadToEnd();
					executejob.StandardError.ReadToEnd();
					UpdateLabel(jstatus, "Load Data Complete");
					if (File.Exists(backupcmdPath))
					{
						File.Delete(backupcmdPath);
					}
					if (File.Exists(loadcmdPath))
					{
						File.Delete(loadcmdPath);
					}
					UpdateLabel(jstatus, "Job Complete");
				}
				catch (Exception ex)
				{
					// migration_log.AppendText("Failed to single thread backup and restore " + oldhost + " or " + newhost + "...");
					//migration_log.AppendText(Environment.NewLine);
					UpdateLabel(jstatus, "No Job Performed");
					Debug.WriteLine("Job failed with exception: " + ex.Message);
				}
			}
			if (string.IsNullOrEmpty(restore))
			{
				Debug.WriteLine("Made it to backup only job execution.");
				int timeout = 4000;
				Ping ping1 = new Ping();
				try
				{
					ping1.Send(oldhost, timeout);
					Debug.WriteLine("Pinging " + oldhost);

					string backupcmdPath = @"\\" + oldhost + @"\c$\temp\Win7Win10_BackupProfile.cmd";

					File.WriteAllBytes(backupcmdPath, WindowsFormsApp1.Properties.Resources.backupcmd);
					string pspath = Path.Combine(Path.GetTempPath(), "psexec.exe");
					string str2 = backup;
					UpdateLabel(jstatus, "Backup Started");
					Process process = new Process()
					{
						StartInfo = {
						UseShellExecute = false,
						RedirectStandardOutput = true,
						RedirectStandardError = true,
						RedirectStandardInput = true,
						FileName = pspath.ToString(),
						Arguments = str2.ToString()
						}
					};
					process.StartInfo.RedirectStandardOutput = true;
					process.Start();
					process.StandardOutput.ReadToEnd();
					process.StandardError.ReadToEnd();
					UpdateLabel(jstatus, "Load completed");
					if (File.Exists(backupcmdPath))
					{
						File.Delete(backupcmdPath);
					}
					UpdateLabel(jstatus, "Job Complete");

				}
				catch (Exception ex)
				{
					//migration_log.AppendText("Failed backup only job for " + oldhost + "...." + ex.Message);
					//migration_log.AppendText(Environment.NewLine);
					Debug.WriteLine(ex);
				}

			}
			if (string.IsNullOrEmpty(backup))
			{
				Ping ping2 = new Ping();
				try
				{
					ping2.Send(newhost, 4000);
					string loadcmdPath = @"\\" + oldhost + @"\c$\temp\Win10_LoadProfile.cmd";
					File.WriteAllBytes(loadcmdPath, WindowsFormsApp1.Properties.Resources.loadcmd);
					string pspath = Path.Combine(Path.GetTempPath(), "psexec.exe");
					string loadjob = restore;
					UpdateLabel(jstatus, "Restore Job Started");
					Process executeJob = new Process()
					{
						StartInfo = {
						UseShellExecute = false,
						RedirectStandardOutput = true,
						RedirectStandardError = true,
						RedirectStandardInput = true,
						FileName = pspath.ToString(),
						Arguments = loadjob.ToString()
						}
					};
					executeJob.StartInfo.RedirectStandardOutput = true;
					executeJob.Start();
					executeJob.StandardOutput.ReadToEnd();
					executeJob.StandardError.ReadToEnd();
					UpdateLabel(jstatus, "Load complete");
					if (File.Exists(loadcmdPath))
					{
						File.Delete(loadcmdPath);
					}
					UpdateLabel(jstatus, "Job Complete");

				}
				catch (Exception ex)
				{
					//migration_log.AppendText("Failed load only job for " + newhost + "...." + ex.Message);
					//migration_log.AppendText(Environment.NewLine);
					Debug.WriteLine("Failed with error " + ex);
				}
			}

		}
		void UpdateLabel(string labelId, string labelText)
		{

			var label = Controls.Find(labelId, true).FirstOrDefault();
			if (label != null)
				this.Invoke(new MethodInvoker(delegate { label.Text = labelText; }));
		}
		private void server1_SelectedIndexChanged(object sender, EventArgs e)
		{
			old01.Enabled = true;
			new01.Enabled = true;
		}

		private void textBox11_TextChanged(object sender, EventArgs e)
		{
			jobstatus01.Visible = false;
			if (new01.Enabled == true)
			{
				start_migration_button.ImageIndex = 9;
			}
		}

		private void migration_job1_SelectedIndexChanged(object sender, EventArgs e)
		{
			server01.Enabled = true;
		}

		private void migration_job2_SelectedIndexChanged(object sender, EventArgs e)
		{
			server2.Enabled = true;
		}

		private void migratino_job3_SelectedIndexChanged(object sender, EventArgs e)
		{
			server3.Enabled = true;
		}

		private void migration_job4_SelectedIndexChanged(object sender, EventArgs e)
		{
			server4.Enabled = true;

		}

		private void migration_job5_SelectedIndexChanged(object sender, EventArgs e)
		{
			server5.Enabled = true;
		}

		private void migration_job6_SelectedIndexChanged(object sender, EventArgs e)
		{
			server6.Enabled = true;

		}

		private void migration_job7_SelectedIndexChanged(object sender, EventArgs e)
		{
			server7.Enabled = true;
		}

		private void migration_job8_SelectedIndexChanged(object sender, EventArgs e)
		{
			server8.Enabled = true;
		}

		private void migration_job9_SelectedIndexChanged(object sender, EventArgs e)
		{
			server9.Enabled = true;
		}

		private void migration_job10_SelectedIndexChanged(object sender, EventArgs e)
		{
			server10.Enabled = true;
		}

		private void server2_SelectedIndexChanged(object sender, EventArgs e)
		{
			old2.Enabled = true;
			new2.Enabled = true;
		}

		private void server3_SelectedIndexChanged(object sender, EventArgs e)
		{
			old3.Enabled = true;
			new3.Enabled = true;
		}

		private void server4_SelectedIndexChanged(object sender, EventArgs e)
		{
			old4.Enabled = true;
			new4.Enabled = true;

		}

		private void server5_SelectedIndexChanged(object sender, EventArgs e)
		{
			old5.Enabled = true;
			new5.Enabled = true;
		}

		private void server6_SelectedIndexChanged(object sender, EventArgs e)
		{
			old6.Enabled = true;
			new6.Enabled = true;
		}

		private void server7_SelectedIndexChanged(object sender, EventArgs e)
		{
			old7.Enabled = true;
			new7.Enabled = true;

		}

		private void server8_SelectedIndexChanged(object sender, EventArgs e)
		{
			old8.Enabled = true;
			new8.Enabled = true;
		}

		private void server9_SelectedIndexChanged(object sender, EventArgs e)
		{
			old9.Enabled = true;
			new9.Enabled = true;
		}

		private void server10_SelectedIndexChanged(object sender, EventArgs e)
		{
			new10.Enabled = true;
			old10.Enabled = true;
		}

		private void copy_job_button_Click(object sender, EventArgs e)
		{
			migration_job2.SelectedIndex = migration_job01.SelectedIndex;
			migration_job3.SelectedIndex = migration_job01.SelectedIndex;
			migration_job4.SelectedIndex = migration_job01.SelectedIndex;
			migration_job5.SelectedIndex = migration_job01.SelectedIndex;
			migration_job6.SelectedIndex = migration_job01.SelectedIndex;
			migration_job7.SelectedIndex = migration_job01.SelectedIndex;
			migration_job8.SelectedIndex = migration_job01.SelectedIndex;
			migration_job9.SelectedIndex = migration_job01.SelectedIndex;
			migration_job10.SelectedIndex = migration_job01.SelectedIndex;
			migration_log.AppendText("Setting all jobs to be the same as " + migration_job01.Text);
			migration_log.AppendText(Environment.NewLine);

		}

		private void button5_Click_1(object sender, EventArgs e)
		{

		}

		private void clear_job_button_Click(object sender, EventArgs e)
		{
			migration_log.AppendText("Clearing all jobs...");
			migration_log.AppendText(Environment.NewLine);
			foreach (Control c in server_jobs_groupbox.Controls)
			{
				if (c is ComboBox)
				{
					if (c.Name.Contains("migration"))
					{
						ComboBox comboBox = (ComboBox)c;
						comboBox.SelectedIndex = -1;
					}
				}
			}

		}

		private void sync_all_groups_button_Click(object sender, EventArgs e)
		{
			List<string> groups = new List<string>();
			foreach (string item in groups_listbox.Items)
			{
				groups.Add(item.ToString());
				Debug.WriteLine("Adding " + item + " to array 'groups'...");
			}
			ADAdd(new_computer.Text, groups);
			new_computer_groups.Enabled = true;
		}

		private void new2_TextChanged(object sender, EventArgs e)
		{
			jobstatus2.Visible = false;
		}

		private void new3_TextChanged(object sender, EventArgs e)
		{
			jobstatus3.Visible = false;
		}

		private void new4_TextChanged(object sender, EventArgs e)
		{
			jobstatus4.Visible = false;

		}

		private void new5_TextChanged(object sender, EventArgs e)
		{
			jobstatus5.Visible = false;
		}

		private void new6_TextChanged(object sender, EventArgs e)
		{
			jobstatus6.Visible = false;
		}

		private void new7_TextChanged(object sender, EventArgs e)
		{
			jobstatus7.Visible = false;
		}

		private void new8_TextChanged(object sender, EventArgs e)
		{
			jobstatus8.Visible = false;
		}

		private void new9_TextChanged(object sender, EventArgs e)
		{
			jobstatus8.Visible = false;
		}

		private void new10_TextChanged(object sender, EventArgs e)
		{
			jobstatus10.Visible = false;
		}

		private void ping_old_host_button_Click(object sender, EventArgs e)
		{
			migration_log.AppendText("Pinging systems...");
			migration_log.AppendText(Environment.NewLine);
			foreach (Control c in server_jobs_groupbox.Controls)
			{
				if (c is TextBox && c.Enabled == true)
				{

					string pc_status = Pinger(c.Text);
					migration_log.AppendText(c.Text + ".....");
					if (pc_status == "Online")
					{
						c.ForeColor = Color.Green;
						migration_log.AppendText(pc_status);
						migration_log.AppendText(Environment.NewLine);

					}
					else
					{
						c.ForeColor = Color.Red;
						migration_log.AppendText(pc_status);
						migration_log.AppendText(Environment.NewLine);
					}
				}
			}
		}

		private void copy_user_groups_SelectedIndexChanged(object sender, EventArgs e)
		{
			remove_from_left_button.Enabled = true;
		}

		private void button6_Click(object sender, EventArgs e)
		{

		}

		private void killHomeDriveToolStripMenuItem_Click(object sender, EventArgs e)
		{
			tabs.TabPages.Clear();
			tabs.TabPages.Add(homedrive);
		}

		private void import_home_drive_users_button_Click(object sender, EventArgs e)
		{
			Stream myStream = null;
			OpenFileDialog openFileDialog1 = new OpenFileDialog();

			openFileDialog1.InitialDirectory = "%userprofile%";
			openFileDialog1.Filter = "txt files (*.txt)|*.txt";
			openFileDialog1.FilterIndex = 2;
			openFileDialog1.RestoreDirectory = true;

			if (openFileDialog1.ShowDialog() == DialogResult.OK)
			{
				try
				{
					if ((myStream = openFileDialog1.OpenFile()) != null)
					{
						using (myStream)
						{
							break_users.Text = File.ReadAllText(openFileDialog1.FileName);
							string trimmed = break_users.Text.ToString();
							break_users.Text = trimmed.Trim();
						}
					}
				}
				catch (Exception ex)
				{
					MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
				}

				myStream.Close();
			}
		}

		private void break_users_TextChanged(object sender, EventArgs e)
		{
			numlines = break_users.Lines.Count();
			break_users.CharacterCasing = CharacterCasing.Upper;
		}

		private void button7_Click_1(object sender, EventArgs e)
		{
			string trimmed = break_users.Text.ToString();
			break_users.Text = trimmed.Trim();
			numlines = break_users.Lines.Count();
			int line = 1;
			string username = "";
			while (line <= numlines)
			{
				home_log.AppendText("Attempting to break Home drive for: " + break_users.Lines[line - 1] + "...");
				home_log.AppendText(Environment.NewLine);
				username = break_users.Lines[line - 1];
				try
				{
					using (PrincipalContext ctx = new PrincipalContext(ContextType.Domain, "lambweston.com", adminuser, adminpass))
					{
						UserPrincipal usr = UserPrincipal.FindByIdentity(ctx, IdentityType.SamAccountName, username);
						if (usr != null)
						{
							if (usr.HomeDirectory != null)
							{
								home_log.AppendText("Found home directory for " + usr + ": " + usr.HomeDirectory.ToString());
								home_log.AppendText(Environment.NewLine);
								usr.HomeDirectory = null;
							}
							else
							{
								home_log.AppendText("No home directory found for " + usr);
								home_log.AppendText(Environment.NewLine);
							}
							if (usr.HomeDrive != null)
							{
								home_log.AppendText("Found home drive for " + usr + ": " + usr.HomeDrive.ToString());
								home_log.AppendText(Environment.NewLine);
								usr.HomeDrive = null;
							}
							else
							{
								home_log.AppendText("No drive found mapped for " + usr);
								home_log.AppendText(Environment.NewLine);
							}
							usr.Save();
							home_log.AppendText("Removed home drive/directory for: " + usr);
							home_log.AppendText(Environment.NewLine);
						}
					}
				}
				catch (System.DirectoryServices.DirectoryServicesCOMException E)
				{
					home_log.AppendText("Something didn't work..." + E.Message);
					home_log.AppendText(Environment.NewLine);
				}
				line++;
			}
		}

		private void lookup_home_drive_button_Click(object sender, EventArgs e)
		{
			string trimmed = break_users.Text.ToString();
			break_users.Text = trimmed.Trim();
			numlines = break_users.Lines.Count();
			int line = 1;
			string username = "";
			while (line <= numlines)
			{
				username = break_users.Lines[line - 1];
				using (PrincipalContext ctx = new PrincipalContext(ContextType.Domain, "lambweston.com", adminuser, adminpass))
				{
					UserPrincipal usr = UserPrincipal.FindByIdentity(ctx, IdentityType.SamAccountName, username);
					if (usr != null)
					{
						if (usr.HomeDirectory != null)
						{
							home_log.AppendText("Found home directory for " + usr + ": " + usr.HomeDirectory.ToString());
							home_log.AppendText(Environment.NewLine);
						}
						else
						{
							home_log.AppendText("No home directory found for " + usr);
							home_log.AppendText(Environment.NewLine);
						}
						if (usr.HomeDrive != null)
						{
							home_log.AppendText("Found home drive for " + usr + ": " + usr.HomeDrive.ToString());
							home_log.AppendText(Environment.NewLine);
						}
						else
						{
							home_log.AppendText("No drive found mapped for " + usr);
							home_log.AppendText(Environment.NewLine);
						}

					}
				}
				line++;
			}
		}
		private string MoveOU(string hostname)
		{
			string logEntry = "";
			string cOU = ComputerDN(hostname);
			string enl = "\r\n";
			if (!cOU.Contains("instance"))
			{
				if (cOU.Contains("Windows_7"))
				{
					string[] split = cOU.Split(new char[] { '/' });
					string eLocation = split[2].ToString();
					Debug.WriteLine(eLocation);
					string[] nsplit = eLocation.Split(new char[] { ',' }, 2);
					string prep = nsplit[1].ToString();
					prep = prep.Replace("Windows_7", "Windows_10");
					Debug.WriteLine(prep);
					DialogResult result = MessageBox.Show("Move computer " + hostname + " to " + prep + "?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
					if (result == DialogResult.Yes)
					{
						try
						{
							DirectoryEntry ol = new DirectoryEntry("LDAP://" + eLocation, adminuser, adminpass);
							DirectoryEntry nl = new DirectoryEntry("LDAP://" + prep, adminuser, adminpass);
							string newName = ol.Name;
							ol.MoveTo(nl, newName);
							ol.Close();
							nl.Close();

							logEntry = "Moved " + hostname + " to Windows 10 OU...\n" + enl + prep + enl;
						}
						catch (Exception ex)
						{
							logEntry = ex.Message;
						}
					}
				}
				else
				{
					logEntry = "Computer " + hostname + " is already in the correct OU..." + enl + cOU + enl;
				}
			}
			else
			{
				logEntry = "Invalid entry." + enl;
			}
			return logEntry;
		}
		private void button8_Click_1(object sender, EventArgs e)
		{
			string logEntry = MoveOU(new_computer.Text);
			log.AppendText(logEntry);
		}

		private void ou_to_move_list_TextChanged(object sender, EventArgs e)
		{
			numlines = ou_to_move_list.Lines.Count();
			ou_to_move_list.CharacterCasing = CharacterCasing.Upper;
		}

		private void mass_move_ou_button_Click(object sender, EventArgs e)
		{
			numlines = ou_to_move_list.Lines.Count();
			int line = 1;
			while (line <= numlines)
			{
				string logEntry = MoveOU(ou_to_move_list.Lines[line - 1]);
				ou_log.AppendText(logEntry);
				ou_log.AppendText(Environment.NewLine);
				line++;
			}
		}
	}
}