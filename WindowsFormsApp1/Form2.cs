﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Threading;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;
using System.Text.RegularExpressions;
using System.IO;



namespace WindowsFormsApp1
{
    public partial class bulk : Form
    {
        public bulk()
        {
            InitializeComponent();
        }



        // PING Function start
        public string Pinger(string hostname)
        {

            string netstatus = "empty";
            Ping ping = new Ping();
            try
            {
                string formatHost = hostname.ToUpper();
                Debug.WriteLine("Pinging " + formatHost + ".....");
                PingReply reply = ping.Send(hostname, 2000);
                if (reply.Status == IPStatus.Success)
                {

                    Debug.WriteLine(hostname + " is online...");
                    netstatus = "Online";
                }
                else
                {
                    Debug.WriteLine(hostname + " is offline...");
                    netstatus = "Offline";
                }
            }
            catch
            {
                Debug.WriteLine("Can't find host " + hostname + "...");
                netstatus = "No DNS Host";
            }
            return netstatus;
        }
        private string GetCurrentDomainPath()
        {
            DirectoryEntry de =
               new DirectoryEntry("LDAP://RootDSE");

            return "LDAP://" +
               de.Properties["defaultNamingContext"][0].
                   ToString();
        }

        int numlines = 0;
        public List<string> ADQ(string hostname)
        {
            try
            {
                var adresults = new List<string>();
                DirectorySearcher ds = null;
                DirectoryEntry de = new
                   DirectoryEntry(GetCurrentDomainPath());
                ds = new DirectorySearcher(de);
                ds.Filter = "(cn=" + hostname + ")";
                ds.PropertiesToLoad.Add("memberOf");
                var result = ds.FindAll();
                if (result != null)
                {
                    Debug.WriteLine("Got AD Results!");
                    foreach (SearchResult sr in result)
                    {
                        if (sr.Properties["memberof"].Count > 0)
                        {
                            Debug.WriteLine("Member of...");

                            foreach (string item in sr.Properties["memberof"])
                            {
                                Debug.WriteLine(item);

                                string cnPattern = @"^CN=(?<cn>.+?)(?<!\\),";
                                string dn = item;
                                Regex re = new Regex(cnPattern);
                                Match m = re.Match(dn);
                                if (m.Success)
                                {
                                    // Item with index 1 returns the first group match.
                                    string cn = m.Groups[1].Value;
                                    adresults.Add(cn);
                                }
                            }
                        }
                    }
                }
                else
                {
                    Debug.WriteLine("This didn't work.");
                }
                return adresults;
            }
            catch
            {
                // do nothing
                List<string> adresults = new List<string>();
                return adresults;
            }

        }



        private void button1_Click(object sender, EventArgs e)
        {
            int line = 1;
            //groups.Items.Clear();
            //groups.Text.ToUpper();
            ping_report.Text = "";
            while (line <= numlines)
            {
                string netstatus = Pinger(computers.Lines[line - 1]);
                List<string> group_list = ADQ(computers.Lines[line - 1]);
                //groups.Items.Add(computers.Lines[line - 1] + "----" + netstatus.ToUpper());
                ping_report.AppendText(computers.Lines[line - 1] + "-" + netstatus.ToUpper());
                ping_report.AppendText(Environment.NewLine);
                if (include_groups_checkbox.Checked == true)
                {
                    foreach (string sr in group_list)
                    {
                        //groups.Items.Add("      |---->" + sr);
                        ping_report.AppendText("    |---->" + sr);
                        ping_report.AppendText(Environment.NewLine);
                    }
                }
                line++;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            numlines = computers.Lines.Count();
            computers.CharacterCasing = CharacterCasing.Upper;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "%userprofile%";
            openFileDialog1.Filter = "txt files (*.txt)|*.txt";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            // Insert code to read the stream here.
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
                computers.Text = File.ReadAllText(openFileDialog1.FileName);
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }
    }
}
