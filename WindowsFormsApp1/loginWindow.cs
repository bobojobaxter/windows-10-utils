﻿using System;
using WindowsFormsApp1.Properties;
using System.Windows.Forms;
using System.DirectoryServices.AccountManagement;
using System.IO;
using CredentialManagement;
using System.Reflection;
using System.Diagnostics;


namespace WindowsFormsApp1
{
    public partial class loginWindow : Form
    {
        public loginWindow()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, args) =>
            {
                string resourceName = new AssemblyName(args.Name).Name + ".dll";
                string resource = Array.Find(this.GetType().Assembly.GetManifestResourceNames(), element => element.EndsWith(resourceName));

                using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resource))
                {
                    Byte[] assemblyData = new Byte[stream.Length];
                    stream.Read(assemblyData, 0, assemblyData.Length);
                    return Assembly.Load(assemblyData);
                }
            };


            InitializeComponent();
            string cPath = @"c:\ProgramData\Lambweston\GroupMembershipTool\ComputerInfo.log";
            string uPath = @"c:\ProgramData\Lambweston\GroupMembershipTool\UserInfo.log";
            username.Text = Settings.Default["defaultuser"].ToString();
            if (!File.Exists(cPath))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(cPath));
                FileStream fsc = new FileStream(cPath, FileMode.CreateNew);
                FileStream fsu = new FileStream(uPath, FileMode.CreateNew);
                fsc.Close();
                fsu.Close();

            }
            CheckCreds();
        }

        private void CheckCreds()
        {
            var cm = new Credential();
            cm.Target = "Win10Utils";
            if (!cm.Exists())
            {
                Debug.WriteLine("No creds found...");
            }
            else
            {
                cm.Load();
                using (PrincipalContext principalCntxt = new PrincipalContext(ContextType.Domain, "lambweston.com"))
                {
                    bool isValid = principalCntxt.ValidateCredentials(cm.Username, cm.Password);
                    if (isValid)
                    {
                        Application.Run(new Win10Utils());
                    }
                    else
                    {
                        Debug.WriteLine("Invalid credentials found.");
                    }
                }
            }
        }
        private void loginWindow_Load(object sender, EventArgs e)
        {
            username.Text = Settings.Default["defaultuser"].ToString();
        }
        public static string user = "";
        public static string pass = "";
        private void checkLogin()
        {
            using (PrincipalContext principalCntxt = new PrincipalContext(ContextType.Domain, "lambweston.com"))
            {
                bool isValid = principalCntxt.ValidateCredentials(username.Text, password.Text);
                if (isValid)
                {
                    Credential saved = new Credential(username.Text, password.Text, "Win10Utils", CredentialType.Generic);
                    saved.PersistanceType = PersistanceType.LocalComputer;
                    saved.Save();
                    user = username.Text;
                    pass = password.Text;
                    Settings.Default["defaultuser"] = username.Text;
                    Settings.Default.Save();
                    Win10Utils m = new Win10Utils();
                    m.FormClosed += new FormClosedEventHandler(m_FormClosed);
                    m.Show();
                    this.Hide();

                }
                else
                {
                    MessageBox.Show("Authentication Failed.\nEither password was entered incorrectly, or cached password is no longer valid.", "Login Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    password.Text = "";
                }
            }
        }
        private void m_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }
        void button1_GotFocus(object sender, EventArgs e)
        {
            button1.NotifyDefault(false);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            checkLogin();
        }

        private void password_TextChanged(object sender, EventArgs e)
        {
            password.UseSystemPasswordChar = true;
        }
        private void password_Leave(object sender, EventArgs e)
        {
            password.Select(0, 0);
        }
        private void password_Enter(object sender, EventArgs e)
        {
            password.SelectAll();

        }
        private void password_KeyDown(object sender, KeyEventArgs e)
        {


            if (e.KeyCode == Keys.Enter)
            {

                checkLogin();
                e.SuppressKeyPress = true;

            }
        }

        private void password_TextChanged_1(object sender, EventArgs e)
        {
            button1.ImageIndex = 2;
        }

        private void password_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
            }
        }

        private void username_Leave(object sender, EventArgs e)
        {
            string user = username.Text;
            try
            {
                if (user[0] != 'a')
                {
                    string fixuser = "a" + user.Substring(1);
                    username.Text = fixuser;
                }
            }
            catch { }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
