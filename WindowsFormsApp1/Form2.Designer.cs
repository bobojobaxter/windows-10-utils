﻿using System.Windows.Forms;

namespace WindowsFormsApp1
{
    partial class bulk
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        //private void groups_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        //{
        //    if (e.Control && e.KeyCode == Keys.C)
        //    {
        //        System.Text.StringBuilder copy_buffer = new System.Text.StringBuilder();
        //        foreach (object item in groups.SelectedItems)
        //            copy_buffer.AppendLine(item.ToString());
        //        if (copy_buffer.Length > 0)
        //            Clipboard.SetText(copy_buffer.ToString());
        //    }
        //}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(bulk));
            this.computers = new System.Windows.Forms.TextBox();
            this.execute_ping = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.include_groups_checkbox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ping_report = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.computer_label = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // computers
            // 
            this.computers.Location = new System.Drawing.Point(17, 71);
            this.computers.Multiline = true;
            this.computers.Name = "computers";
            this.computers.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.computers.Size = new System.Drawing.Size(111, 488);
            this.computers.TabIndex = 0;
            this.computers.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // execute_ping
            // 
            this.execute_ping.Location = new System.Drawing.Point(138, 36);
            this.execute_ping.Name = "execute_ping";
            this.execute_ping.Size = new System.Drawing.Size(252, 26);
            this.execute_ping.TabIndex = 1;
            this.execute_ping.Text = "Ping Systems";
            this.execute_ping.UseVisualStyleBackColor = true;
            this.execute_ping.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.include_groups_checkbox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.ping_report);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.computer_label);
            this.groupBox1.Controls.Add(this.computers);
            this.groupBox1.Controls.Add(this.execute_ping);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(405, 569);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bulk System Info";
            // 
            // include_groups_checkbox
            // 
            this.include_groups_checkbox.AutoSize = true;
            this.include_groups_checkbox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.include_groups_checkbox.Location = new System.Drawing.Point(292, 13);
            this.include_groups_checkbox.Name = "include_groups_checkbox";
            this.include_groups_checkbox.Size = new System.Drawing.Size(98, 17);
            this.include_groups_checkbox.TabIndex = 7;
            this.include_groups_checkbox.Text = "Include Groups";
            this.include_groups_checkbox.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 6;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ping_report
            // 
            this.ping_report.Location = new System.Drawing.Point(138, 71);
            this.ping_report.Multiline = true;
            this.ping_report.Name = "ping_report";
            this.ping_report.ReadOnly = true;
            this.ping_report.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ping_report.Size = new System.Drawing.Size(252, 492);
            this.ping_report.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(17, 36);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(111, 26);
            this.button1.TabIndex = 4;
            this.button1.Text = "Load List";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // computer_label
            // 
            this.computer_label.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.computer_label.Location = new System.Drawing.Point(17, 16);
            this.computer_label.Name = "computer_label";
            this.computer_label.Size = new System.Drawing.Size(100, 23);
            this.computer_label.TabIndex = 3;
            this.computer_label.Text = "Computer List";
            this.computer_label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // bulk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 625);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "bulk";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Bulk System Status";
            this.TopMost = true;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox computers;
        private System.Windows.Forms.Button execute_ping;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label computer_label;
        private System.Windows.Forms.Button button1;
        private TextBox ping_report;
        private CheckBox include_groups_checkbox;
        private Label label1;
    }
}