@Echo Off

:SetSCDP
SET SCDPServer=%1




:OSVersion
FOR /F "tokens=2 delims==" %%A IN ('WMIC Path Win32_Processor Get AddressWidth /Format:list') DO SET OSB=%%A
if "%OSB%"=="32" goto 32b
if "%OSB%"=="64" goto 64b
goto ERR



:32b
if exist "\\%SCDPServer%\USMT\Data\%COMPUTERNAME%" ( rmdir "\\%SCDPServer%\USMT\Data\%COMPUTERNAME%" /s /q )
powershell -executionpolicy bypass -inputformat none -File "\\%SCDPServer%\USMT\ConfigFiles\Remove_CAG_Profiles.ps1"
"\\%SCDPServer%\USMT\ConfigFiles\x32\scanstate.exe" "\\%SCDPServer%\USMT\Data\%COMPUTERNAME%" /i:"\\%SCDPServer%\USMT\ConfigFiles\x32\migapp.xml" /i:"\\%SCDPServer%\USMT\ConfigFiles\x32\miguser.xml" /l:"C:\temp\Scan.log" /v:5 /progress:"C:\temp\prog.log" /o /localonly /c /r:3 /w:5 /ue:%computername%\* /ue:lw\a* /uel:30
rename "C:\temp\Scan.log" "%COMPUTERNAME%-Scan.log"
xcopy "C:\temp\%COMPUTERNAME%-Scan.log" "\\%SCDPServer%\USMT\Logs\" /q /y
GOTO EX



:64b
if exist "\\%SCDPServer%\USMT\Data\%COMPUTERNAME%" ( rmdir "\\%SCDPServer%\USMT\Data\%COMPUTERNAME%" /s /q )
powershell -executionpolicy bypass -inputformat none -File "\\%SCDPServer%\USMT\ConfigFiles\Remove_CAG_Profiles.ps1"
"\\%SCDPServer%\USMT\ConfigFiles\x64\scanstate.exe" "\\%SCDPServer%\USMT\Data\%COMPUTERNAME%" /i:"\\%SCDPServer%\USMT\ConfigFiles\x64\migapp.xml" /i:"\\%SCDPServer%\USMT\ConfigFiles\x64\miguser.xml" /i:"\\%SCDPServer%\USMT\ConfigFiles\x64\printers.xml"  /l:"C:\temp\Scan.log" /v:5 /progress:"C:\temp\prog.log" /o /localonly /c /r:3 /w:5 /ue:%computername%\* /ue:lw\a* /uel:30
rename "C:\temp\Scan.log" "%COMPUTERNAME%-Scan.log"
xcopy "C:\temp\%COMPUTERNAME%-Scan.log" "\\%SCDPServer%\USMT\Logs\" /q /y
GOTO EX



:ERR
ECHO ERROR Running Profile Backup
del "C:\temp\Scan.log" /s /q
del "C:\temp\prog.log" /s /q
del "C:\temp\%COMPUTERNAME%-Scan.log" /s /q
exit 1



:EX
ECHO COMPLETE
del "C:\temp\Scan.log" /s /q
del "C:\temp\prog.log" /s /q
del "C:\temp\%COMPUTERNAME%-Scan.log" /s /q
@ping "%SCDPServer%.lambweston.com" -n 6 -w 100
exit 0