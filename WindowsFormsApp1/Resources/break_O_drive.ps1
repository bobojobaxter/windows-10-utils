﻿$filepath = $PSCommandPath | Split-Path -Parent
echo $filepath
$users = ForEach ($user in $(Get-Content $filepath'\odrive.txt')) 
    {
        try
            {
               set-aduser $user -HomeDrive $null -HomeDirectory $null
               write-host "User $user homedrive removed."
             }
        
        catch

            {
               write-host "User $user couldn't remove homedrive"
            }

   }


