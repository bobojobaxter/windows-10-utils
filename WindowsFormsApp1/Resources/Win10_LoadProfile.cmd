@ECHO OFF

SET SCDPServer=%1
SET COMPUTERNAME=%2

ECHO Checking for "\\%SCDPServer%\USMT\Data\%COMPUTERNAME%\"
IF EXIST "\\%SCDPServer%\USMT\Data\%COMPUTERNAME%\" ( GOTO RUNN )
ELSE (
Echo %COMPUTERNAME% not Found on %SCDPServer%
GOTO ERR
)


:RUNN
ECHO MIG File found for %COMPUTERNAME%. Running Load State.
FOR /F "tokens=2 delims==" %%A IN ('WMIC Path Win32_Processor Get AddressWidth /Format:list') DO SET OSB=%%A
if "%OSB%"=="32" goto 32b
if "%OSB%"=="64" goto 64b
goto ERR


:32b
ECHO 32 Bit
"\\%SCDPServer%\USMT\ConfigFiles\x86\loadstate.exe" "\\%SCDPServer%\USMT\Data\%COMPUTERNAME%" /i:"\\%SCDPServer%\USMT\ConfigFiles\x86\migapp.xml" /i:"\\%SCDPServer%\USMT\ConfigFiles\x86\miguser.xml" /l:"C:\temp\Load.log" /v:5 /progress:"C:\temp\Loadprog.log" /c /r:3 /w:5 /ue:"lw\a* /uel:30
rename "C:\temp\Load.log" "%COMPUTERNAME%-Load.log"
xcopy "C:\temp\%COMPUTERNAME%-Load.log" "\\%SCDPServer%\USMT\Logs\" /q /y
mkdir "C:\Users\Public\Desktop\Windows 10 - Post Upgrade Instructions\" /f
xcopy "\\wakensccm01.lambweston.com\USMT\ConfigFiles\Windows 10 - Post Upgrade Instructions\*" "C:\Users\Public\Desktop\Windows 10 - Post Upgrade Instructions\" /h/i/c/k/e/r/y
GOTO EX


:64b
ECHO 64 Bit
"\\%SCDPServer%\USMT\ConfigFiles\x64\loadstate.exe" "\\%SCDPServer%\USMT\Data\%COMPUTERNAME%" /i:"\\%SCDPServer%\USMT\ConfigFiles\x64\migapp.xml" /i:"\\%SCDPServer%\USMT\ConfigFiles\x64\miguser.xml" /i:"\\%SCDPServer%\USMT\ConfigFiles\x64\printers.xml" /l:"C:\temp\Load.log" /v:5 /progress:"C:\temp\Loadprog.log" /c /r:3 /w:5 /ue:"lw\a* /uel:30
rename "C:\temp\Load.log" "%COMPUTERNAME%-Load.log"
xcopy "C:\temp\%COMPUTERNAME%-Load.log" "\\%SCDPServer%\USMT\Logs\" /q /y
mkdir "C:\Users\Public\Desktop\Windows 10 - Post Upgrade Instructions\" /f
xcopy "\\wakensccm01.lambweston.com\USMT\ConfigFiles\Windows 10 - Post Upgrade Instructions\*" "C:\Users\Public\Desktop\Windows 10 - Post Upgrade Instructions\" /h/i/c/k/e/r/y
GOTO EX

:ERR
ECHO ERROR running Scan State
del "C:\temp\Load.log" /s /q
del "C:\temp\Loadprog.log" /s /q
del "C:\temp\%COMPUTERNAME%-Load.log" /s /q
exit 1

:EX
ECHO COMPLETE
del "C:\temp\Load.log" /s /q
del "C:\temp\Loadprog.log" /s /q
del "C:\temp\%COMPUTERNAME%-Load.log" /s /q
@ping "%SCDPServer%.lambweston.com" -n 6 -w 100
exit 0
