﻿namespace WindowsFormsApp1
{
    partial class Win10Utils
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Win10Utils));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fIleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetFormToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.computerGroupsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.userGroupsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pingReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.migration_tooltip = new System.Windows.Forms.ToolStripMenuItem();
            this.killHomeDriveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.old_computer_label = new System.Windows.Forms.Label();
            this.old_computer = new System.Windows.Forms.TextBox();
            this.new_computer = new System.Windows.Forms.TextBox();
            this.new_computer_label = new System.Windows.Forms.Label();
            this.lookup_groups_button = new System.Windows.Forms.Button();
            this.groups_listbox = new System.Windows.Forms.ListBox();
            this.old_system_status = new System.Windows.Forms.Label();
            this.system_status = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.username = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.password = new System.Windows.Forms.TextBox();
            this.directorySearcher = new System.DirectoryServices.DirectorySearcher();
            this.sync_groups = new System.Windows.Forms.Button();
            this.new_computer_groups = new System.Windows.Forms.ListBox();
            this.new_system_status = new System.Windows.Forms.Label();
            this.directoryEntry = new System.DirectoryServices.DirectoryEntry();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.sync_all_groups_button = new System.Windows.Forms.Button();
            this.remove_right = new System.Windows.Forms.Button();
            this.tabs = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.desc_group = new System.Windows.Forms.GroupBox();
            this.move_ou_button = new System.Windows.Forms.Button();
            this.imgButton = new System.Windows.Forms.ImageList(this.components);
            this.copy_desc = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.new_desc = new System.Windows.Forms.TextBox();
            this.set_desc = new System.Windows.Forms.Button();
            this.computer_description = new System.Windows.Forms.TextBox();
            this.logbox = new System.Windows.Forms.GroupBox();
            this.log = new System.Windows.Forms.TextBox();
            this.adminCreds = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.user_log = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.remove_from_left_button = new System.Windows.Forms.Button();
            this.remove_from_right_button = new System.Windows.Forms.Button();
            this.copy_user_groups = new System.Windows.Forms.ListBox();
            this.add_user_groups = new System.Windows.Forms.ListBox();
            this.add_user_groups_button = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.user2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.User1 = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.ou_to_move_list = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.mass_move_ou_button = new System.Windows.Forms.Button();
            this.ou_log = new System.Windows.Forms.RichTextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ping_report = new System.Windows.Forms.RichTextBox();
            this.include_groups_checkbox = new System.Windows.Forms.CheckBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.computer_list = new System.Windows.Forms.TextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.migration = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.migration_log = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.server_jobs_groupbox = new System.Windows.Forms.GroupBox();
            this.migration_job10 = new System.Windows.Forms.ComboBox();
            this.migration_job01 = new System.Windows.Forms.ComboBox();
            this.old4 = new System.Windows.Forms.TextBox();
            this.migration_job6 = new System.Windows.Forms.ComboBox();
            this.server8 = new System.Windows.Forms.ComboBox();
            this.server9 = new System.Windows.Forms.ComboBox();
            this.migration_job9 = new System.Windows.Forms.ComboBox();
            this.old5 = new System.Windows.Forms.TextBox();
            this.old3 = new System.Windows.Forms.TextBox();
            this.migration_job8 = new System.Windows.Forms.ComboBox();
            this.server7 = new System.Windows.Forms.ComboBox();
            this.new01 = new System.Windows.Forms.TextBox();
            this.server10 = new System.Windows.Forms.ComboBox();
            this.migration_job7 = new System.Windows.Forms.ComboBox();
            this.old6 = new System.Windows.Forms.TextBox();
            this.new4 = new System.Windows.Forms.TextBox();
            this.old2 = new System.Windows.Forms.TextBox();
            this.migration_job5 = new System.Windows.Forms.ComboBox();
            this.server6 = new System.Windows.Forms.ComboBox();
            this.new5 = new System.Windows.Forms.TextBox();
            this.old01 = new System.Windows.Forms.TextBox();
            this.migration_job4 = new System.Windows.Forms.ComboBox();
            this.old7 = new System.Windows.Forms.TextBox();
            this.new3 = new System.Windows.Forms.TextBox();
            this.server5 = new System.Windows.Forms.ComboBox();
            this.migration_job3 = new System.Windows.Forms.ComboBox();
            this.old8 = new System.Windows.Forms.TextBox();
            this.new6 = new System.Windows.Forms.TextBox();
            this.server4 = new System.Windows.Forms.ComboBox();
            this.migration_job2 = new System.Windows.Forms.ComboBox();
            this.jobstatus01 = new System.Windows.Forms.Label();
            this.new2 = new System.Windows.Forms.TextBox();
            this.old9 = new System.Windows.Forms.TextBox();
            this.jobstatus2 = new System.Windows.Forms.Label();
            this.new7 = new System.Windows.Forms.TextBox();
            this.server3 = new System.Windows.Forms.ComboBox();
            this.jobstatus10 = new System.Windows.Forms.Label();
            this.jobstatus3 = new System.Windows.Forms.Label();
            this.new8 = new System.Windows.Forms.TextBox();
            this.old10 = new System.Windows.Forms.TextBox();
            this.jobstatus9 = new System.Windows.Forms.Label();
            this.jobstatus4 = new System.Windows.Forms.Label();
            this.jobstatus8 = new System.Windows.Forms.Label();
            this.server2 = new System.Windows.Forms.ComboBox();
            this.new9 = new System.Windows.Forms.TextBox();
            this.jobstatus5 = new System.Windows.Forms.Label();
            this.jobstatus7 = new System.Windows.Forms.Label();
            this.new10 = new System.Windows.Forms.TextBox();
            this.server01 = new System.Windows.Forms.ComboBox();
            this.jobstatus6 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.ping_old_host_button = new System.Windows.Forms.Button();
            this.copy_job_button = new System.Windows.Forms.Button();
            this.clear_job_button = new System.Windows.Forms.Button();
            this.clearserversbutton = new System.Windows.Forms.Button();
            this.copyserversbutton = new System.Windows.Forms.Button();
            this.start_migration_button = new System.Windows.Forms.Button();
            this.paste_from_excel_button = new System.Windows.Forms.Button();
            this.homedrive = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.home_log = new System.Windows.Forms.RichTextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.lookup_home_drive_button = new System.Windows.Forms.Button();
            this.break_users = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.largebuttons = new System.Windows.Forms.ImageList(this.components);
            this.import_home_drive_users_button = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.username1 = new System.Windows.Forms.TextBox();
            this.password1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.load_migration_list = new System.Windows.Forms.OpenFileDialog();
            this.process1 = new System.Diagnostics.Process();
            this.copy_from_excel_tooltip = new System.Windows.Forms.ToolTip(this.components);
            this.copy_server_tooltip = new System.Windows.Forms.ToolTip(this.components);
            this.clear_servers = new System.Windows.Forms.ToolTip(this.components);
            this.start_batch = new System.Windows.Forms.ToolTip(this.components);
            this.ping = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabs.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.desc_group.SuspendLayout();
            this.logbox.SuspendLayout();
            this.adminCreds.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.migration.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.server_jobs_groupbox.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.homedrive.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fIleToolStripMenuItem,
            this.editToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(631, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fIleToolStripMenuItem
            // 
            this.fIleToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fIleToolStripMenuItem.Name = "fIleToolStripMenuItem";
            this.fIleToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fIleToolStripMenuItem.Text = "FIle";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resetFormToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // resetFormToolStripMenuItem
            // 
            this.resetFormToolStripMenuItem.Name = "resetFormToolStripMenuItem";
            this.resetFormToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.resetFormToolStripMenuItem.Text = "Reset Form";
            this.resetFormToolStripMenuItem.Click += new System.EventHandler(this.resetFormToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.computerGroupsToolStripMenuItem1,
            this.userGroupsToolStripMenuItem1,
            this.pingReportToolStripMenuItem1,
            this.migration_tooltip,
            this.killHomeDriveToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // computerGroupsToolStripMenuItem1
            // 
            this.computerGroupsToolStripMenuItem1.Name = "computerGroupsToolStripMenuItem1";
            this.computerGroupsToolStripMenuItem1.Size = new System.Drawing.Size(169, 22);
            this.computerGroupsToolStripMenuItem1.Text = "Computer Groups";
            this.computerGroupsToolStripMenuItem1.Click += new System.EventHandler(this.computerGroupsToolStripMenuItem_Click);
            // 
            // userGroupsToolStripMenuItem1
            // 
            this.userGroupsToolStripMenuItem1.Name = "userGroupsToolStripMenuItem1";
            this.userGroupsToolStripMenuItem1.Size = new System.Drawing.Size(169, 22);
            this.userGroupsToolStripMenuItem1.Text = "User Groups";
            this.userGroupsToolStripMenuItem1.Click += new System.EventHandler(this.userGroupsToolStripMenuItem_Click);
            // 
            // pingReportToolStripMenuItem1
            // 
            this.pingReportToolStripMenuItem1.Name = "pingReportToolStripMenuItem1";
            this.pingReportToolStripMenuItem1.Size = new System.Drawing.Size(169, 22);
            this.pingReportToolStripMenuItem1.Text = "Bulk Tools";
            this.pingReportToolStripMenuItem1.Click += new System.EventHandler(this.pingReportToolStripMenuItem1_Click);
            // 
            // migration_tooltip
            // 
            this.migration_tooltip.Name = "migration_tooltip";
            this.migration_tooltip.Size = new System.Drawing.Size(169, 22);
            this.migration_tooltip.Text = "Profile Migration";
            this.migration_tooltip.Click += new System.EventHandler(this.profileMigrationToolStripMenuItem_Click);
            // 
            // killHomeDriveToolStripMenuItem
            // 
            this.killHomeDriveToolStripMenuItem.Name = "killHomeDriveToolStripMenuItem";
            this.killHomeDriveToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.killHomeDriveToolStripMenuItem.Text = "Kill Home Drive";
            this.killHomeDriveToolStripMenuItem.Click += new System.EventHandler(this.killHomeDriveToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // old_computer_label
            // 
            this.old_computer_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.old_computer_label.AutoSize = true;
            this.old_computer_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.old_computer_label.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.old_computer_label.Location = new System.Drawing.Point(44, 18);
            this.old_computer_label.Name = "old_computer_label";
            this.old_computer_label.Size = new System.Drawing.Size(74, 13);
            this.old_computer_label.TabIndex = 0;
            this.old_computer_label.Text = "Old Computer:";
            this.old_computer_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // old_computer
            // 
            this.old_computer.AcceptsTab = true;
            this.old_computer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.old_computer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.old_computer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.old_computer.Location = new System.Drawing.Point(44, 35);
            this.old_computer.Name = "old_computer";
            this.old_computer.Size = new System.Drawing.Size(110, 20);
            this.old_computer.TabIndex = 1;
            this.old_computer.TextChanged += new System.EventHandler(this.old_computer_TextChanged);
            // 
            // new_computer
            // 
            this.new_computer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.new_computer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.new_computer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.new_computer.Location = new System.Drawing.Point(191, 35);
            this.new_computer.Name = "new_computer";
            this.new_computer.Size = new System.Drawing.Size(110, 20);
            this.new_computer.TabIndex = 2;
            this.new_computer.TextChanged += new System.EventHandler(this.new_computer_TextChanged);
            this.new_computer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.new_computer_KeyDown);
            // 
            // new_computer_label
            // 
            this.new_computer_label.AutoSize = true;
            this.new_computer_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.new_computer_label.Location = new System.Drawing.Point(191, 18);
            this.new_computer_label.Name = "new_computer_label";
            this.new_computer_label.Size = new System.Drawing.Size(80, 13);
            this.new_computer_label.TabIndex = 2;
            this.new_computer_label.Text = "New Computer:";
            this.new_computer_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.new_computer_label.Click += new System.EventHandler(this.new_computer_label_Click);
            // 
            // lookup_groups_button
            // 
            this.lookup_groups_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookup_groups_button.Location = new System.Drawing.Point(215, 33);
            this.lookup_groups_button.Name = "lookup_groups_button";
            this.lookup_groups_button.Size = new System.Drawing.Size(165, 30);
            this.lookup_groups_button.TabIndex = 3;
            this.lookup_groups_button.Text = "Lookup Groups";
            this.lookup_groups_button.UseVisualStyleBackColor = true;
            this.lookup_groups_button.Click += new System.EventHandler(this.lookup_groups_button_Click);
            // 
            // groups_listbox
            // 
            this.groups_listbox.FormattingEnabled = true;
            this.groups_listbox.Location = new System.Drawing.Point(22, 33);
            this.groups_listbox.Name = "groups_listbox";
            this.groups_listbox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.groups_listbox.Size = new System.Drawing.Size(190, 147);
            this.groups_listbox.TabIndex = 6;
            this.groups_listbox.SelectedIndexChanged += new System.EventHandler(this.groups_listbox_SelectedIndexChanged);
            // 
            // old_system_status
            // 
            this.old_system_status.AutoSize = true;
            this.old_system_status.Enabled = false;
            this.old_system_status.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.old_system_status.Location = new System.Drawing.Point(14, 73);
            this.old_system_status.Name = "old_system_status";
            this.old_system_status.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.old_system_status.Size = new System.Drawing.Size(0, 13);
            this.old_system_status.TabIndex = 7;
            // 
            // system_status
            // 
            this.system_status.AutoSize = true;
            this.system_status.Enabled = false;
            this.system_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.system_status.Location = new System.Drawing.Point(41, 57);
            this.system_status.Name = "system_status";
            this.system_status.Size = new System.Drawing.Size(103, 17);
            this.system_status.TabIndex = 8;
            this.system_status.Text = "Network Status";
            this.system_status.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.system_status.Visible = false;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label1.Location = new System.Drawing.Point(39, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Username:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // username
            // 
            this.username.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.username.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.username.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
            this.username.Location = new System.Drawing.Point(103, 4);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(94, 20);
            this.username.TabIndex = 10;
            this.username.Text = "lw\\";
            this.username.TextChanged += new System.EventHandler(this.username_TextChanged);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label2.Location = new System.Drawing.Point(41, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Password:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // password
            // 
            this.password.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.password.Location = new System.Drawing.Point(103, 34);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(94, 20);
            this.password.TabIndex = 12;
            this.password.UseSystemPasswordChar = true;
            this.password.TextChanged += new System.EventHandler(this.password_TextChanged);
            // 
            // directorySearcher
            // 
            this.directorySearcher.ClientTimeout = System.TimeSpan.Parse("-00:00:01");
            this.directorySearcher.ServerPageTimeLimit = System.TimeSpan.Parse("-00:00:01");
            this.directorySearcher.ServerTimeLimit = System.TimeSpan.Parse("-00:00:01");
            // 
            // sync_groups
            // 
            this.sync_groups.Enabled = false;
            this.sync_groups.Location = new System.Drawing.Point(215, 70);
            this.sync_groups.Name = "sync_groups";
            this.sync_groups.Size = new System.Drawing.Size(165, 30);
            this.sync_groups.TabIndex = 4;
            this.sync_groups.Text = "Add Selected Groups -->";
            this.sync_groups.UseVisualStyleBackColor = true;
            this.sync_groups.Click += new System.EventHandler(this.sync_groups_Click);
            // 
            // new_computer_groups
            // 
            this.new_computer_groups.FormattingEnabled = true;
            this.new_computer_groups.Location = new System.Drawing.Point(386, 33);
            this.new_computer_groups.Name = "new_computer_groups";
            this.new_computer_groups.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.new_computer_groups.Size = new System.Drawing.Size(190, 147);
            this.new_computer_groups.TabIndex = 15;
            this.new_computer_groups.SelectedIndexChanged += new System.EventHandler(this.new_computer_groups_SelectedIndexChanged);
            // 
            // new_system_status
            // 
            this.new_system_status.AutoSize = true;
            this.new_system_status.Enabled = false;
            this.new_system_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.new_system_status.Location = new System.Drawing.Point(188, 58);
            this.new_system_status.Name = "new_system_status";
            this.new_system_status.Size = new System.Drawing.Size(103, 17);
            this.new_system_status.TabIndex = 18;
            this.new_system_status.Text = "Network Status";
            this.new_system_status.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.new_system_status.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.new_computer);
            this.groupBox1.Controls.Add(this.new_system_status);
            this.groupBox1.Controls.Add(this.system_status);
            this.groupBox1.Controls.Add(this.new_computer_label);
            this.groupBox1.Controls.Add(this.old_computer_label);
            this.groupBox1.Controls.Add(this.old_computer);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(359, 93);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Computer Info";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.sync_all_groups_button);
            this.groupBox2.Controls.Add(this.remove_right);
            this.groupBox2.Controls.Add(this.groups_listbox);
            this.groupBox2.Controls.Add(this.new_computer_groups);
            this.groupBox2.Controls.Add(this.lookup_groups_button);
            this.groupBox2.Controls.Add(this.sync_groups);
            this.groupBox2.Location = new System.Drawing.Point(9, 206);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(597, 196);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Group Membership Info";
            // 
            // sync_all_groups_button
            // 
            this.sync_all_groups_button.Location = new System.Drawing.Point(215, 144);
            this.sync_all_groups_button.Name = "sync_all_groups_button";
            this.sync_all_groups_button.Size = new System.Drawing.Size(165, 30);
            this.sync_all_groups_button.TabIndex = 17;
            this.sync_all_groups_button.Text = "Sync All from Left";
            this.sync_all_groups_button.UseVisualStyleBackColor = true;
            this.sync_all_groups_button.Click += new System.EventHandler(this.sync_all_groups_button_Click);
            // 
            // remove_right
            // 
            this.remove_right.Enabled = false;
            this.remove_right.Location = new System.Drawing.Point(215, 107);
            this.remove_right.Name = "remove_right";
            this.remove_right.Size = new System.Drawing.Size(165, 30);
            this.remove_right.TabIndex = 16;
            this.remove_right.Text = "Remove from new computer";
            this.remove_right.UseVisualStyleBackColor = true;
            this.remove_right.Click += new System.EventHandler(this.remove_right_Click);
            // 
            // tabs
            // 
            this.tabs.Controls.Add(this.tabPage1);
            this.tabs.Controls.Add(this.tabPage2);
            this.tabs.Controls.Add(this.tabPage3);
            this.tabs.Controls.Add(this.migration);
            this.tabs.Controls.Add(this.homedrive);
            this.tabs.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabs.Location = new System.Drawing.Point(0, 31);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(631, 648);
            this.tabs.TabIndex = 21;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Transparent;
            this.tabPage1.Controls.Add(this.desc_group);
            this.tabPage1.Controls.Add(this.logbox);
            this.tabPage1.Controls.Add(this.adminCreds);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(623, 622);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Computer Information";
            // 
            // desc_group
            // 
            this.desc_group.Controls.Add(this.move_ou_button);
            this.desc_group.Controls.Add(this.copy_desc);
            this.desc_group.Controls.Add(this.label4);
            this.desc_group.Controls.Add(this.label3);
            this.desc_group.Controls.Add(this.new_desc);
            this.desc_group.Controls.Add(this.set_desc);
            this.desc_group.Controls.Add(this.computer_description);
            this.desc_group.Location = new System.Drawing.Point(6, 106);
            this.desc_group.Name = "desc_group";
            this.desc_group.Size = new System.Drawing.Size(597, 94);
            this.desc_group.TabIndex = 26;
            this.desc_group.TabStop = false;
            this.desc_group.Text = "Description";
            // 
            // move_ou_button
            // 
            this.move_ou_button.Enabled = false;
            this.move_ou_button.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.move_ou_button.ImageIndex = 12;
            this.move_ou_button.ImageList = this.imgButton;
            this.move_ou_button.Location = new System.Drawing.Point(389, 53);
            this.move_ou_button.Name = "move_ou_button";
            this.move_ou_button.Size = new System.Drawing.Size(165, 40);
            this.move_ou_button.TabIndex = 6;
            this.move_ou_button.Text = "Check OU";
            this.move_ou_button.UseVisualStyleBackColor = true;
            this.move_ou_button.Click += new System.EventHandler(this.button8_Click_1);
            // 
            // imgButton
            // 
            this.imgButton.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgButton.ImageStream")));
            this.imgButton.TransparentColor = System.Drawing.Color.Transparent;
            this.imgButton.Images.SetKeyName(0, "copy.png");
            this.imgButton.Images.SetKeyName(1, "ui-12-512.png");
            this.imgButton.Images.SetKeyName(2, "clip-art-red-dot-clipart-1.jpg");
            this.imgButton.Images.SetKeyName(3, "greendot.png");
            this.imgButton.Images.SetKeyName(4, "refresh.png");
            this.imgButton.Images.SetKeyName(5, "export_to_excel.png");
            this.imgButton.Images.SetKeyName(6, "edit-clear.png");
            this.imgButton.Images.SetKeyName(7, "Button Play.png");
            this.imgButton.Images.SetKeyName(8, "Paste.png");
            this.imgButton.Images.SetKeyName(9, "green play.png");
            this.imgButton.Images.SetKeyName(10, "network_ethernet.png");
            this.imgButton.Images.SetKeyName(11, "Import.png");
            this.imgButton.Images.SetKeyName(12, "magnifying_glass.png");
            this.imgButton.Images.SetKeyName(13, "arrow-right.png");
            // 
            // copy_desc
            // 
            this.copy_desc.FlatAppearance.BorderSize = 0;
            this.copy_desc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.copy_desc.ImageIndex = 0;
            this.copy_desc.ImageList = this.imgButton;
            this.copy_desc.Location = new System.Drawing.Point(335, 31);
            this.copy_desc.Name = "copy_desc";
            this.copy_desc.Size = new System.Drawing.Size(32, 32);
            this.copy_desc.TabIndex = 5;
            this.copy_desc.UseVisualStyleBackColor = true;
            this.copy_desc.Click += new System.EventHandler(this.copy_desc_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "New Computer:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Old Computer:";
            // 
            // new_desc
            // 
            this.new_desc.Enabled = false;
            this.new_desc.Location = new System.Drawing.Point(103, 53);
            this.new_desc.Name = "new_desc";
            this.new_desc.Size = new System.Drawing.Size(226, 20);
            this.new_desc.TabIndex = 2;
            this.new_desc.TextChanged += new System.EventHandler(this.computer_description_TextChanged);
            // 
            // set_desc
            // 
            this.set_desc.Enabled = false;
            this.set_desc.Location = new System.Drawing.Point(389, 7);
            this.set_desc.Name = "set_desc";
            this.set_desc.Size = new System.Drawing.Size(165, 35);
            this.set_desc.TabIndex = 1;
            this.set_desc.Text = "Set Description";
            this.set_desc.UseVisualStyleBackColor = true;
            this.set_desc.Click += new System.EventHandler(this.set_desc_Click);
            // 
            // computer_description
            // 
            this.computer_description.Enabled = false;
            this.computer_description.Location = new System.Drawing.Point(103, 22);
            this.computer_description.Name = "computer_description";
            this.computer_description.Size = new System.Drawing.Size(226, 20);
            this.computer_description.TabIndex = 0;
            this.computer_description.Text = "<Name of User> - <PC Type/Function>";
            this.computer_description.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // logbox
            // 
            this.logbox.Controls.Add(this.log);
            this.logbox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.logbox.Location = new System.Drawing.Point(3, 426);
            this.logbox.Name = "logbox";
            this.logbox.Size = new System.Drawing.Size(617, 193);
            this.logbox.TabIndex = 25;
            this.logbox.TabStop = false;
            this.logbox.Text = "Log";
            // 
            // log
            // 
            this.log.AcceptsReturn = true;
            this.log.BackColor = System.Drawing.SystemColors.Info;
            this.log.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.log.Location = new System.Drawing.Point(7, 19);
            this.log.Multiline = true;
            this.log.Name = "log";
            this.log.ReadOnly = true;
            this.log.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.log.Size = new System.Drawing.Size(584, 162);
            this.log.TabIndex = 0;
            this.log.TabStop = false;
            this.log.WordWrap = false;
            this.log.TextChanged += new System.EventHandler(this.log_TextChanged);
            // 
            // adminCreds
            // 
            this.adminCreds.Controls.Add(this.tableLayoutPanel1);
            this.adminCreds.Location = new System.Drawing.Point(371, 6);
            this.adminCreds.Name = "adminCreds";
            this.adminCreds.Size = new System.Drawing.Size(232, 93);
            this.adminCreds.TabIndex = 23;
            this.adminCreds.TabStop = false;
            this.adminCreds.Text = "Admin Credentials";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.username, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.password, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(17, 24);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(200, 59);
            this.tableLayoutPanel1.TabIndex = 22;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(623, 622);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "User Information";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.user_log);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox6.Location = new System.Drawing.Point(3, 447);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(617, 172);
            this.groupBox6.TabIndex = 27;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Log";
            // 
            // user_log
            // 
            this.user_log.AcceptsReturn = true;
            this.user_log.BackColor = System.Drawing.SystemColors.Info;
            this.user_log.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.user_log.Location = new System.Drawing.Point(7, 19);
            this.user_log.Multiline = true;
            this.user_log.Name = "user_log";
            this.user_log.ReadOnly = true;
            this.user_log.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.user_log.Size = new System.Drawing.Size(584, 147);
            this.user_log.TabIndex = 0;
            this.user_log.TabStop = false;
            this.user_log.WordWrap = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button3);
            this.groupBox5.Controls.Add(this.button2);
            this.groupBox5.Controls.Add(this.remove_from_left_button);
            this.groupBox5.Controls.Add(this.remove_from_right_button);
            this.groupBox5.Controls.Add(this.copy_user_groups);
            this.groupBox5.Controls.Add(this.add_user_groups);
            this.groupBox5.Controls.Add(this.add_user_groups_button);
            this.groupBox5.Location = new System.Drawing.Point(6, 140);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(597, 298);
            this.groupBox5.TabIndex = 26;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Group Membership Info";
            // 
            // button3
            // 
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ImageIndex = 4;
            this.button3.ImageList = this.imgButton;
            this.button3.Location = new System.Drawing.Point(64, 248);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 38);
            this.button3.TabIndex = 19;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ImageIndex = 4;
            this.button2.ImageList = this.imgButton;
            this.button2.Location = new System.Drawing.Point(460, 248);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 38);
            this.button2.TabIndex = 18;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // remove_from_left_button
            // 
            this.remove_from_left_button.Enabled = false;
            this.remove_from_left_button.Location = new System.Drawing.Point(214, 91);
            this.remove_from_left_button.Name = "remove_from_left_button";
            this.remove_from_left_button.Size = new System.Drawing.Size(165, 30);
            this.remove_from_left_button.TabIndex = 17;
            this.remove_from_left_button.Text = "Remove from left";
            this.remove_from_left_button.UseVisualStyleBackColor = true;
            this.remove_from_left_button.Click += new System.EventHandler(this.remove_from_left_button_Click);
            // 
            // remove_from_right_button
            // 
            this.remove_from_right_button.Enabled = false;
            this.remove_from_right_button.Location = new System.Drawing.Point(214, 55);
            this.remove_from_right_button.Name = "remove_from_right_button";
            this.remove_from_right_button.Size = new System.Drawing.Size(165, 30);
            this.remove_from_right_button.TabIndex = 16;
            this.remove_from_right_button.Text = "Remove from right";
            this.remove_from_right_button.UseVisualStyleBackColor = true;
            this.remove_from_right_button.Click += new System.EventHandler(this.remove_from_right_button_Click);
            // 
            // copy_user_groups
            // 
            this.copy_user_groups.FormattingEnabled = true;
            this.copy_user_groups.Location = new System.Drawing.Point(21, 17);
            this.copy_user_groups.Name = "copy_user_groups";
            this.copy_user_groups.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.copy_user_groups.Size = new System.Drawing.Size(190, 225);
            this.copy_user_groups.Sorted = true;
            this.copy_user_groups.TabIndex = 6;
            this.copy_user_groups.SelectedIndexChanged += new System.EventHandler(this.copy_user_groups_SelectedIndexChanged);
            this.copy_user_groups.SelectedValueChanged += new System.EventHandler(this.copy_user_groups_SelectedValueChanged);
            // 
            // add_user_groups
            // 
            this.add_user_groups.FormattingEnabled = true;
            this.add_user_groups.Location = new System.Drawing.Point(385, 17);
            this.add_user_groups.Name = "add_user_groups";
            this.add_user_groups.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.add_user_groups.Size = new System.Drawing.Size(190, 225);
            this.add_user_groups.Sorted = true;
            this.add_user_groups.TabIndex = 15;
            this.add_user_groups.SelectedIndexChanged += new System.EventHandler(this.add_user_groups_SelectedIndexChanged);
            // 
            // add_user_groups_button
            // 
            this.add_user_groups_button.Enabled = false;
            this.add_user_groups_button.Location = new System.Drawing.Point(214, 19);
            this.add_user_groups_button.Name = "add_user_groups_button";
            this.add_user_groups_button.Size = new System.Drawing.Size(165, 30);
            this.add_user_groups_button.TabIndex = 4;
            this.add_user_groups_button.Text = "Add Selected Groups -->";
            this.add_user_groups_button.UseVisualStyleBackColor = true;
            this.add_user_groups_button.Click += new System.EventHandler(this.add_user_groups_button_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button1);
            this.groupBox4.Controls.Add(this.comboBox2);
            this.groupBox4.Controls.Add(this.comboBox1);
            this.groupBox4.Controls.Add(this.user2);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.User1);
            this.groupBox4.Location = new System.Drawing.Point(6, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(597, 128);
            this.groupBox4.TabIndex = 24;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "User Lookup";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(216, 92);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(165, 30);
            this.button1.TabIndex = 5;
            this.button1.Text = "Lookup Users";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox2
            // 
            this.comboBox2.Enabled = false;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(345, 60);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(190, 21);
            this.comboBox2.TabIndex = 4;
            this.comboBox2.Text = "Select User...";
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.lookup_user_to_add_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.Enabled = false;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(64, 60);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(190, 21);
            this.comboBox1.TabIndex = 3;
            this.comboBox1.Text = "Select User...";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.lookup_user_to_copy_Click);
            // 
            // user2
            // 
            this.user2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.user2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
            this.user2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.user2.Location = new System.Drawing.Point(345, 34);
            this.user2.Name = "user2";
            this.user2.Size = new System.Drawing.Size(190, 20);
            this.user2.TabIndex = 2;
            this.user2.TextChanged += new System.EventHandler(this.user2_TextChanged);
            this.user2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.User1_KeyDown);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label9.Location = new System.Drawing.Point(344, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(216, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "User to Add Permission: (Last Name Search)";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.label10.Location = new System.Drawing.Point(61, 17);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(168, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "User to Copy: (Last Name Search)";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // User1
            // 
            this.User1.AcceptsTab = true;
            this.User1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.User1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
            this.User1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.User1.Location = new System.Drawing.Point(64, 34);
            this.User1.Name = "User1";
            this.User1.Size = new System.Drawing.Size(190, 20);
            this.User1.TabIndex = 1;
            this.User1.TextChanged += new System.EventHandler(this.User1_TextChanged);
            this.User1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.User1_KeyDown);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.groupBox11);
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(623, 622);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Bulk Tools";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.ou_to_move_list);
            this.groupBox11.Controls.Add(this.label15);
            this.groupBox11.Controls.Add(this.label14);
            this.groupBox11.Controls.Add(this.mass_move_ou_button);
            this.groupBox11.Controls.Add(this.ou_log);
            this.groupBox11.Location = new System.Drawing.Point(7, 326);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(595, 290);
            this.groupBox11.TabIndex = 6;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Move OU";
            // 
            // ou_to_move_list
            // 
            this.ou_to_move_list.Location = new System.Drawing.Point(9, 40);
            this.ou_to_move_list.Multiline = true;
            this.ou_to_move_list.Name = "ou_to_move_list";
            this.ou_to_move_list.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.ou_to_move_list.Size = new System.Drawing.Size(154, 244);
            this.ou_to_move_list.TabIndex = 5;
            this.ou_to_move_list.TextChanged += new System.EventHandler(this.ou_to_move_list_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 21);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "Computer List";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(229, 21);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "Results";
            // 
            // mass_move_ou_button
            // 
            this.mass_move_ou_button.FlatAppearance.BorderSize = 0;
            this.mass_move_ou_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mass_move_ou_button.ImageIndex = 13;
            this.mass_move_ou_button.ImageList = this.imgButton;
            this.mass_move_ou_button.Location = new System.Drawing.Point(180, 117);
            this.mass_move_ou_button.Name = "mass_move_ou_button";
            this.mass_move_ou_button.Size = new System.Drawing.Size(32, 32);
            this.mass_move_ou_button.TabIndex = 2;
            this.mass_move_ou_button.TabStop = false;
            this.mass_move_ou_button.UseVisualStyleBackColor = true;
            this.mass_move_ou_button.Click += new System.EventHandler(this.mass_move_ou_button_Click);
            // 
            // ou_log
            // 
            this.ou_log.BackColor = System.Drawing.SystemColors.Info;
            this.ou_log.Location = new System.Drawing.Point(229, 40);
            this.ou_log.Name = "ou_log";
            this.ou_log.ReadOnly = true;
            this.ou_log.Size = new System.Drawing.Size(359, 244);
            this.ou_log.TabIndex = 1;
            this.ou_log.Text = "";
            this.ou_log.WordWrap = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ping_report);
            this.groupBox3.Controls.Add(this.include_groups_checkbox);
            this.groupBox3.Controls.Add(this.button5);
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Controls.Add(this.computer_list);
            this.groupBox3.Controls.Add(this.button6);
            this.groupBox3.Location = new System.Drawing.Point(7, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(595, 314);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Ping Report";
            // 
            // ping_report
            // 
            this.ping_report.Location = new System.Drawing.Point(185, 60);
            this.ping_report.Name = "ping_report";
            this.ping_report.ReadOnly = true;
            this.ping_report.Size = new System.Drawing.Size(404, 238);
            this.ping_report.TabIndex = 6;
            this.ping_report.Text = "";
            this.ping_report.WordWrap = false;
            // 
            // include_groups_checkbox
            // 
            this.include_groups_checkbox.AutoSize = true;
            this.include_groups_checkbox.Location = new System.Drawing.Point(266, 22);
            this.include_groups_checkbox.Name = "include_groups_checkbox";
            this.include_groups_checkbox.Size = new System.Drawing.Size(98, 17);
            this.include_groups_checkbox.TabIndex = 4;
            this.include_groups_checkbox.Text = "Include Groups";
            this.include_groups_checkbox.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(185, 19);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 3;
            this.button5.Text = "Ping";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(53, 22);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(110, 23);
            this.button4.TabIndex = 0;
            this.button4.Text = "Browse...";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // computer_list
            // 
            this.computer_list.Location = new System.Drawing.Point(9, 60);
            this.computer_list.Multiline = true;
            this.computer_list.Name = "computer_list";
            this.computer_list.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.computer_list.Size = new System.Drawing.Size(154, 238);
            this.computer_list.TabIndex = 1;
            this.computer_list.TextChanged += new System.EventHandler(this.computer_list_TextChanged);
            // 
            // button6
            // 
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.ImageIndex = 5;
            this.button6.ImageList = this.imgButton;
            this.button6.Location = new System.Drawing.Point(534, 7);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(55, 47);
            this.button6.TabIndex = 5;
            this.button6.TabStop = false;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Visible = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // migration
            // 
            this.migration.BackColor = System.Drawing.SystemColors.Control;
            this.migration.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.migration.Controls.Add(this.groupBox9);
            this.migration.Controls.Add(this.label12);
            this.migration.Controls.Add(this.label11);
            this.migration.Controls.Add(this.label8);
            this.migration.Controls.Add(this.label7);
            this.migration.Controls.Add(this.server_jobs_groupbox);
            this.migration.Controls.Add(this.groupBox7);
            this.migration.Location = new System.Drawing.Point(4, 22);
            this.migration.Name = "migration";
            this.migration.Padding = new System.Windows.Forms.Padding(3);
            this.migration.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.migration.Size = new System.Drawing.Size(609, 622);
            this.migration.TabIndex = 3;
            this.migration.Text = "Profile Migration";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.migration_log);
            this.groupBox9.Location = new System.Drawing.Point(7, 450);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(592, 162);
            this.groupBox9.TabIndex = 6;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Log";
            // 
            // migration_log
            // 
            this.migration_log.AcceptsReturn = true;
            this.migration_log.BackColor = System.Drawing.SystemColors.Info;
            this.migration_log.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.migration_log.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.migration_log.Location = new System.Drawing.Point(6, 19);
            this.migration_log.Multiline = true;
            this.migration_log.Name = "migration_log";
            this.migration_log.ReadOnly = true;
            this.migration_log.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.migration_log.Size = new System.Drawing.Size(580, 131);
            this.migration_log.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(402, 79);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(76, 17);
            this.label12.TabIndex = 5;
            this.label12.Text = "New Host";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(283, 79);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 17);
            this.label11.TabIndex = 4;
            this.label11.Text = "Old Host";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(149, 79);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(133, 17);
            this.label8.TabIndex = 3;
            this.label8.Text = "Distribution Point";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(20, 78);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 17);
            this.label7.TabIndex = 2;
            this.label7.Text = "Job To Perform";
            // 
            // server_jobs_groupbox
            // 
            this.server_jobs_groupbox.Controls.Add(this.migration_job10);
            this.server_jobs_groupbox.Controls.Add(this.migration_job01);
            this.server_jobs_groupbox.Controls.Add(this.old4);
            this.server_jobs_groupbox.Controls.Add(this.migration_job6);
            this.server_jobs_groupbox.Controls.Add(this.server8);
            this.server_jobs_groupbox.Controls.Add(this.server9);
            this.server_jobs_groupbox.Controls.Add(this.migration_job9);
            this.server_jobs_groupbox.Controls.Add(this.old5);
            this.server_jobs_groupbox.Controls.Add(this.old3);
            this.server_jobs_groupbox.Controls.Add(this.migration_job8);
            this.server_jobs_groupbox.Controls.Add(this.server7);
            this.server_jobs_groupbox.Controls.Add(this.new01);
            this.server_jobs_groupbox.Controls.Add(this.server10);
            this.server_jobs_groupbox.Controls.Add(this.migration_job7);
            this.server_jobs_groupbox.Controls.Add(this.old6);
            this.server_jobs_groupbox.Controls.Add(this.new4);
            this.server_jobs_groupbox.Controls.Add(this.old2);
            this.server_jobs_groupbox.Controls.Add(this.migration_job5);
            this.server_jobs_groupbox.Controls.Add(this.server6);
            this.server_jobs_groupbox.Controls.Add(this.new5);
            this.server_jobs_groupbox.Controls.Add(this.old01);
            this.server_jobs_groupbox.Controls.Add(this.migration_job4);
            this.server_jobs_groupbox.Controls.Add(this.old7);
            this.server_jobs_groupbox.Controls.Add(this.new3);
            this.server_jobs_groupbox.Controls.Add(this.server5);
            this.server_jobs_groupbox.Controls.Add(this.migration_job3);
            this.server_jobs_groupbox.Controls.Add(this.old8);
            this.server_jobs_groupbox.Controls.Add(this.new6);
            this.server_jobs_groupbox.Controls.Add(this.server4);
            this.server_jobs_groupbox.Controls.Add(this.migration_job2);
            this.server_jobs_groupbox.Controls.Add(this.jobstatus01);
            this.server_jobs_groupbox.Controls.Add(this.new2);
            this.server_jobs_groupbox.Controls.Add(this.old9);
            this.server_jobs_groupbox.Controls.Add(this.jobstatus2);
            this.server_jobs_groupbox.Controls.Add(this.new7);
            this.server_jobs_groupbox.Controls.Add(this.server3);
            this.server_jobs_groupbox.Controls.Add(this.jobstatus10);
            this.server_jobs_groupbox.Controls.Add(this.jobstatus3);
            this.server_jobs_groupbox.Controls.Add(this.new8);
            this.server_jobs_groupbox.Controls.Add(this.old10);
            this.server_jobs_groupbox.Controls.Add(this.jobstatus9);
            this.server_jobs_groupbox.Controls.Add(this.jobstatus4);
            this.server_jobs_groupbox.Controls.Add(this.jobstatus8);
            this.server_jobs_groupbox.Controls.Add(this.server2);
            this.server_jobs_groupbox.Controls.Add(this.new9);
            this.server_jobs_groupbox.Controls.Add(this.jobstatus5);
            this.server_jobs_groupbox.Controls.Add(this.jobstatus7);
            this.server_jobs_groupbox.Controls.Add(this.new10);
            this.server_jobs_groupbox.Controls.Add(this.server01);
            this.server_jobs_groupbox.Controls.Add(this.jobstatus6);
            this.server_jobs_groupbox.Location = new System.Drawing.Point(7, 82);
            this.server_jobs_groupbox.Name = "server_jobs_groupbox";
            this.server_jobs_groupbox.Size = new System.Drawing.Size(592, 361);
            this.server_jobs_groupbox.TabIndex = 1;
            this.server_jobs_groupbox.TabStop = false;
            // 
            // migration_job10
            // 
            this.migration_job10.FormattingEnabled = true;
            this.migration_job10.Items.AddRange(new object[] {
            "Backup and Load Profile",
            "Backup Profile to Server",
            "Load Existing Profile on New PC"});
            this.migration_job10.Location = new System.Drawing.Point(16, 334);
            this.migration_job10.Name = "migration_job10";
            this.migration_job10.Size = new System.Drawing.Size(121, 21);
            this.migration_job10.TabIndex = 54;
            this.migration_job10.SelectedIndexChanged += new System.EventHandler(this.migration_job10_SelectedIndexChanged);
            // 
            // migration_job01
            // 
            this.migration_job01.FormattingEnabled = true;
            this.migration_job01.Items.AddRange(new object[] {
            "Backup and Load Profile",
            "Backup Profile to Server",
            "Load Existing Profile on New PC"});
            this.migration_job01.Location = new System.Drawing.Point(16, 19);
            this.migration_job01.Name = "migration_job01";
            this.migration_job01.Size = new System.Drawing.Size(121, 21);
            this.migration_job01.TabIndex = 45;
            this.migration_job01.SelectedIndexChanged += new System.EventHandler(this.migration_job1_SelectedIndexChanged);
            // 
            // old4
            // 
            this.old4.Enabled = false;
            this.old4.Location = new System.Drawing.Point(279, 124);
            this.old4.Name = "old4";
            this.old4.Size = new System.Drawing.Size(100, 20);
            this.old4.TabIndex = 4;
            // 
            // migration_job6
            // 
            this.migration_job6.FormattingEnabled = true;
            this.migration_job6.Items.AddRange(new object[] {
            "Backup and Load Profile",
            "Backup Profile to Server",
            "Load Existing Profile on New PC"});
            this.migration_job6.Location = new System.Drawing.Point(16, 193);
            this.migration_job6.Name = "migration_job6";
            this.migration_job6.Size = new System.Drawing.Size(121, 21);
            this.migration_job6.TabIndex = 53;
            this.migration_job6.SelectedIndexChanged += new System.EventHandler(this.migration_job6_SelectedIndexChanged);
            // 
            // server8
            // 
            this.server8.Enabled = false;
            this.server8.FormattingEnabled = true;
            this.server8.Items.AddRange(new object[] {
            "ABTABSCDP01",
            "IDAMESCDP01",
            "IDEAGSCDP01",
            "IDTWISCDP01",
            "ILNAPSCDP01",
            "LADELSCDP01",
            "ORBO3SCDP01",
            "ORBOASCDP01",
            "WACONSCDP01",
            "WAKENSCCM01",
            "WAKENSCDP01",
            "WAKENSCDP02",
            "WAPA2SCDP01",
            "WAPASSCDP01",
            "WAPT2SCDP01",
            "WAPTNSCDP01",
            "WAQUISCDP01",
            "WARICSCDP01",
            "WAWARSCDP01"});
            this.server8.Location = new System.Drawing.Point(145, 263);
            this.server8.Name = "server8";
            this.server8.Size = new System.Drawing.Size(121, 21);
            this.server8.Sorted = true;
            this.server8.TabIndex = 29;
            this.server8.TabStop = false;
            this.server8.SelectedIndexChanged += new System.EventHandler(this.server8_SelectedIndexChanged);
            // 
            // server9
            // 
            this.server9.Enabled = false;
            this.server9.FormattingEnabled = true;
            this.server9.Items.AddRange(new object[] {
            "ABTABSCDP01",
            "IDAMESCDP01",
            "IDEAGSCDP01",
            "IDTWISCDP01",
            "ILNAPSCDP01",
            "LADELSCDP01",
            "ORBO3SCDP01",
            "ORBOASCDP01",
            "WACONSCDP01",
            "WAKENSCCM01",
            "WAKENSCDP01",
            "WAKENSCDP02",
            "WAPA2SCDP01",
            "WAPASSCDP01",
            "WAPT2SCDP01",
            "WAPTNSCDP01",
            "WAQUISCDP01",
            "WARICSCDP01",
            "WAWARSCDP01"});
            this.server9.Location = new System.Drawing.Point(145, 298);
            this.server9.Name = "server9";
            this.server9.Size = new System.Drawing.Size(121, 21);
            this.server9.Sorted = true;
            this.server9.TabIndex = 30;
            this.server9.TabStop = false;
            this.server9.SelectedIndexChanged += new System.EventHandler(this.server9_SelectedIndexChanged);
            // 
            // migration_job9
            // 
            this.migration_job9.FormattingEnabled = true;
            this.migration_job9.Items.AddRange(new object[] {
            "Backup and Load Profile",
            "Backup Profile to Server",
            "Load Existing Profile on New PC"});
            this.migration_job9.Location = new System.Drawing.Point(16, 299);
            this.migration_job9.Name = "migration_job9";
            this.migration_job9.Size = new System.Drawing.Size(121, 21);
            this.migration_job9.TabIndex = 52;
            this.migration_job9.SelectedIndexChanged += new System.EventHandler(this.migration_job9_SelectedIndexChanged);
            // 
            // old5
            // 
            this.old5.Enabled = false;
            this.old5.Location = new System.Drawing.Point(280, 159);
            this.old5.Name = "old5";
            this.old5.Size = new System.Drawing.Size(100, 20);
            this.old5.TabIndex = 5;
            // 
            // old3
            // 
            this.old3.Enabled = false;
            this.old3.Location = new System.Drawing.Point(280, 89);
            this.old3.Name = "old3";
            this.old3.Size = new System.Drawing.Size(100, 20);
            this.old3.TabIndex = 3;
            // 
            // migration_job8
            // 
            this.migration_job8.FormattingEnabled = true;
            this.migration_job8.Items.AddRange(new object[] {
            "Backup and Load Profile",
            "Backup Profile to Server",
            "Load Existing Profile on New PC"});
            this.migration_job8.Location = new System.Drawing.Point(16, 264);
            this.migration_job8.Name = "migration_job8";
            this.migration_job8.Size = new System.Drawing.Size(121, 21);
            this.migration_job8.TabIndex = 51;
            this.migration_job8.SelectedIndexChanged += new System.EventHandler(this.migration_job8_SelectedIndexChanged);
            // 
            // server7
            // 
            this.server7.Enabled = false;
            this.server7.FormattingEnabled = true;
            this.server7.Items.AddRange(new object[] {
            "ABTABSCDP01",
            "IDAMESCDP01",
            "IDEAGSCDP01",
            "IDTWISCDP01",
            "ILNAPSCDP01",
            "LADELSCDP01",
            "ORBO3SCDP01",
            "ORBOASCDP01",
            "WACONSCDP01",
            "WAKENSCCM01",
            "WAKENSCDP01",
            "WAKENSCDP02",
            "WAPA2SCDP01",
            "WAPASSCDP01",
            "WAPT2SCDP01",
            "WAPTNSCDP01",
            "WAQUISCDP01",
            "WARICSCDP01",
            "WAWARSCDP01"});
            this.server7.Location = new System.Drawing.Point(145, 228);
            this.server7.Name = "server7";
            this.server7.Size = new System.Drawing.Size(121, 21);
            this.server7.Sorted = true;
            this.server7.TabIndex = 28;
            this.server7.TabStop = false;
            this.server7.SelectedIndexChanged += new System.EventHandler(this.server7_SelectedIndexChanged);
            // 
            // new01
            // 
            this.new01.Enabled = false;
            this.new01.Location = new System.Drawing.Point(398, 19);
            this.new01.Name = "new01";
            this.new01.Size = new System.Drawing.Size(100, 20);
            this.new01.TabIndex = 20;
            this.new01.TextChanged += new System.EventHandler(this.textBox11_TextChanged);
            // 
            // server10
            // 
            this.server10.Enabled = false;
            this.server10.FormattingEnabled = true;
            this.server10.Items.AddRange(new object[] {
            "ABTABSCDP01",
            "IDAMESCDP01",
            "IDEAGSCDP01",
            "IDTWISCDP01",
            "ILNAPSCDP01",
            "LADELSCDP01",
            "ORBO3SCDP01",
            "ORBOASCDP01",
            "WACONSCDP01",
            "WAKENSCCM01",
            "WAKENSCDP01",
            "WAKENSCDP02",
            "WAPA2SCDP01",
            "WAPASSCDP01",
            "WAPT2SCDP01",
            "WAPTNSCDP01",
            "WAQUISCDP01",
            "WARICSCDP01",
            "WAWARSCDP01"});
            this.server10.Location = new System.Drawing.Point(145, 333);
            this.server10.Name = "server10";
            this.server10.Size = new System.Drawing.Size(121, 21);
            this.server10.Sorted = true;
            this.server10.TabIndex = 31;
            this.server10.TabStop = false;
            this.server10.SelectedIndexChanged += new System.EventHandler(this.server10_SelectedIndexChanged);
            // 
            // migration_job7
            // 
            this.migration_job7.FormattingEnabled = true;
            this.migration_job7.Items.AddRange(new object[] {
            "Backup and Load Profile",
            "Backup Profile to Server",
            "Load Existing Profile on New PC"});
            this.migration_job7.Location = new System.Drawing.Point(16, 229);
            this.migration_job7.Name = "migration_job7";
            this.migration_job7.Size = new System.Drawing.Size(121, 21);
            this.migration_job7.TabIndex = 50;
            this.migration_job7.SelectedIndexChanged += new System.EventHandler(this.migration_job7_SelectedIndexChanged);
            // 
            // old6
            // 
            this.old6.Enabled = false;
            this.old6.Location = new System.Drawing.Point(280, 194);
            this.old6.Name = "old6";
            this.old6.Size = new System.Drawing.Size(100, 20);
            this.old6.TabIndex = 6;
            // 
            // new4
            // 
            this.new4.Enabled = false;
            this.new4.Location = new System.Drawing.Point(398, 124);
            this.new4.Name = "new4";
            this.new4.Size = new System.Drawing.Size(100, 20);
            this.new4.TabIndex = 17;
            this.new4.TextChanged += new System.EventHandler(this.new4_TextChanged);
            // 
            // old2
            // 
            this.old2.Enabled = false;
            this.old2.Location = new System.Drawing.Point(280, 54);
            this.old2.Name = "old2";
            this.old2.Size = new System.Drawing.Size(100, 20);
            this.old2.TabIndex = 2;
            // 
            // migration_job5
            // 
            this.migration_job5.FormattingEnabled = true;
            this.migration_job5.Items.AddRange(new object[] {
            "Backup and Load Profile",
            "Backup Profile to Server",
            "Load Existing Profile on New PC"});
            this.migration_job5.Location = new System.Drawing.Point(16, 159);
            this.migration_job5.Name = "migration_job5";
            this.migration_job5.Size = new System.Drawing.Size(121, 21);
            this.migration_job5.TabIndex = 49;
            this.migration_job5.SelectedIndexChanged += new System.EventHandler(this.migration_job5_SelectedIndexChanged);
            // 
            // server6
            // 
            this.server6.Enabled = false;
            this.server6.FormattingEnabled = true;
            this.server6.Items.AddRange(new object[] {
            "ABTABSCDP01",
            "IDAMESCDP01",
            "IDEAGSCDP01",
            "IDTWISCDP01",
            "ILNAPSCDP01",
            "LADELSCDP01",
            "ORBO3SCDP01",
            "ORBOASCDP01",
            "WACONSCDP01",
            "WAKENSCCM01",
            "WAKENSCDP01",
            "WAKENSCDP02",
            "WAPA2SCDP01",
            "WAPASSCDP01",
            "WAPT2SCDP01",
            "WAPTNSCDP01",
            "WAQUISCDP01",
            "WARICSCDP01",
            "WAWARSCDP01"});
            this.server6.Location = new System.Drawing.Point(145, 193);
            this.server6.Name = "server6";
            this.server6.Size = new System.Drawing.Size(121, 21);
            this.server6.Sorted = true;
            this.server6.TabIndex = 27;
            this.server6.TabStop = false;
            this.server6.SelectedIndexChanged += new System.EventHandler(this.server6_SelectedIndexChanged);
            // 
            // new5
            // 
            this.new5.Enabled = false;
            this.new5.Location = new System.Drawing.Point(398, 159);
            this.new5.Name = "new5";
            this.new5.Size = new System.Drawing.Size(100, 20);
            this.new5.TabIndex = 16;
            this.new5.TextChanged += new System.EventHandler(this.new5_TextChanged);
            // 
            // old01
            // 
            this.old01.Enabled = false;
            this.old01.Location = new System.Drawing.Point(280, 18);
            this.old01.Name = "old01";
            this.old01.Size = new System.Drawing.Size(100, 20);
            this.old01.TabIndex = 1;
            // 
            // migration_job4
            // 
            this.migration_job4.FormattingEnabled = true;
            this.migration_job4.Items.AddRange(new object[] {
            "Backup and Load Profile",
            "Backup Profile to Server",
            "Load Existing Profile on New PC"});
            this.migration_job4.Location = new System.Drawing.Point(16, 124);
            this.migration_job4.Name = "migration_job4";
            this.migration_job4.Size = new System.Drawing.Size(121, 21);
            this.migration_job4.TabIndex = 48;
            this.migration_job4.SelectedIndexChanged += new System.EventHandler(this.migration_job4_SelectedIndexChanged);
            // 
            // old7
            // 
            this.old7.Enabled = false;
            this.old7.Location = new System.Drawing.Point(280, 229);
            this.old7.Name = "old7";
            this.old7.Size = new System.Drawing.Size(100, 20);
            this.old7.TabIndex = 7;
            // 
            // new3
            // 
            this.new3.Enabled = false;
            this.new3.Location = new System.Drawing.Point(398, 89);
            this.new3.Name = "new3";
            this.new3.Size = new System.Drawing.Size(100, 20);
            this.new3.TabIndex = 18;
            this.new3.TextChanged += new System.EventHandler(this.new3_TextChanged);
            // 
            // server5
            // 
            this.server5.Enabled = false;
            this.server5.FormattingEnabled = true;
            this.server5.Items.AddRange(new object[] {
            "ABTABSCDP01",
            "IDAMESCDP01",
            "IDEAGSCDP01",
            "IDTWISCDP01",
            "ILNAPSCDP01",
            "LADELSCDP01",
            "ORBO3SCDP01",
            "ORBOASCDP01",
            "WACONSCDP01",
            "WAKENSCCM01",
            "WAKENSCDP01",
            "WAKENSCDP02",
            "WAPA2SCDP01",
            "WAPASSCDP01",
            "WAPT2SCDP01",
            "WAPTNSCDP01",
            "WAQUISCDP01",
            "WARICSCDP01",
            "WAWARSCDP01"});
            this.server5.Location = new System.Drawing.Point(145, 158);
            this.server5.Name = "server5";
            this.server5.Size = new System.Drawing.Size(121, 21);
            this.server5.Sorted = true;
            this.server5.TabIndex = 26;
            this.server5.TabStop = false;
            this.server5.SelectedIndexChanged += new System.EventHandler(this.server5_SelectedIndexChanged);
            // 
            // migration_job3
            // 
            this.migration_job3.FormattingEnabled = true;
            this.migration_job3.Items.AddRange(new object[] {
            "Backup and Load Profile",
            "Backup Profile to Server",
            "Load Existing Profile on New PC"});
            this.migration_job3.Location = new System.Drawing.Point(16, 89);
            this.migration_job3.Name = "migration_job3";
            this.migration_job3.Size = new System.Drawing.Size(121, 21);
            this.migration_job3.TabIndex = 47;
            this.migration_job3.SelectedIndexChanged += new System.EventHandler(this.migratino_job3_SelectedIndexChanged);
            // 
            // old8
            // 
            this.old8.Enabled = false;
            this.old8.Location = new System.Drawing.Point(280, 264);
            this.old8.Name = "old8";
            this.old8.Size = new System.Drawing.Size(100, 20);
            this.old8.TabIndex = 8;
            // 
            // new6
            // 
            this.new6.Enabled = false;
            this.new6.Location = new System.Drawing.Point(398, 194);
            this.new6.Name = "new6";
            this.new6.Size = new System.Drawing.Size(100, 20);
            this.new6.TabIndex = 15;
            this.new6.TextChanged += new System.EventHandler(this.new6_TextChanged);
            // 
            // server4
            // 
            this.server4.Enabled = false;
            this.server4.FormattingEnabled = true;
            this.server4.Items.AddRange(new object[] {
            "ABTABSCDP01",
            "IDAMESCDP01",
            "IDEAGSCDP01",
            "IDTWISCDP01",
            "ILNAPSCDP01",
            "LADELSCDP01",
            "ORBO3SCDP01",
            "ORBOASCDP01",
            "WACONSCDP01",
            "WAKENSCCM01",
            "WAKENSCDP01",
            "WAKENSCDP02",
            "WAPA2SCDP01",
            "WAPASSCDP01",
            "WAPT2SCDP01",
            "WAPTNSCDP01",
            "WAQUISCDP01",
            "WARICSCDP01",
            "WAWARSCDP01"});
            this.server4.Location = new System.Drawing.Point(145, 123);
            this.server4.Name = "server4";
            this.server4.Size = new System.Drawing.Size(121, 21);
            this.server4.Sorted = true;
            this.server4.TabIndex = 25;
            this.server4.TabStop = false;
            this.server4.SelectedIndexChanged += new System.EventHandler(this.server4_SelectedIndexChanged);
            // 
            // migration_job2
            // 
            this.migration_job2.FormattingEnabled = true;
            this.migration_job2.Items.AddRange(new object[] {
            "Backup and Load Profile",
            "Backup Profile to Server",
            "Load Existing Profile on New PC"});
            this.migration_job2.Location = new System.Drawing.Point(16, 54);
            this.migration_job2.Name = "migration_job2";
            this.migration_job2.Size = new System.Drawing.Size(121, 21);
            this.migration_job2.TabIndex = 46;
            this.migration_job2.SelectedIndexChanged += new System.EventHandler(this.migration_job2_SelectedIndexChanged);
            // 
            // jobstatus01
            // 
            this.jobstatus01.AutoSize = true;
            this.jobstatus01.Location = new System.Drawing.Point(504, 22);
            this.jobstatus01.Name = "jobstatus01";
            this.jobstatus01.Size = new System.Drawing.Size(66, 13);
            this.jobstatus01.TabIndex = 35;
            this.jobstatus01.Text = "Job 1 Status";
            this.jobstatus01.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.jobstatus01.Visible = false;
            // 
            // new2
            // 
            this.new2.Enabled = false;
            this.new2.Location = new System.Drawing.Point(398, 54);
            this.new2.Name = "new2";
            this.new2.Size = new System.Drawing.Size(100, 20);
            this.new2.TabIndex = 19;
            this.new2.TextChanged += new System.EventHandler(this.new2_TextChanged);
            // 
            // old9
            // 
            this.old9.Enabled = false;
            this.old9.Location = new System.Drawing.Point(280, 299);
            this.old9.Name = "old9";
            this.old9.Size = new System.Drawing.Size(100, 20);
            this.old9.TabIndex = 9;
            // 
            // jobstatus2
            // 
            this.jobstatus2.AutoSize = true;
            this.jobstatus2.Location = new System.Drawing.Point(504, 57);
            this.jobstatus2.Name = "jobstatus2";
            this.jobstatus2.Size = new System.Drawing.Size(66, 13);
            this.jobstatus2.TabIndex = 36;
            this.jobstatus2.Text = "Job 2 Status";
            this.jobstatus2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.jobstatus2.Visible = false;
            // 
            // new7
            // 
            this.new7.Enabled = false;
            this.new7.Location = new System.Drawing.Point(398, 229);
            this.new7.Name = "new7";
            this.new7.Size = new System.Drawing.Size(100, 20);
            this.new7.TabIndex = 14;
            this.new7.TextChanged += new System.EventHandler(this.new7_TextChanged);
            // 
            // server3
            // 
            this.server3.Enabled = false;
            this.server3.FormattingEnabled = true;
            this.server3.Items.AddRange(new object[] {
            "ABTABSCDP01",
            "IDAMESCDP01",
            "IDEAGSCDP01",
            "IDTWISCDP01",
            "ILNAPSCDP01",
            "LADELSCDP01",
            "ORBO3SCDP01",
            "ORBOASCDP01",
            "WACONSCDP01",
            "WAKENSCCM01",
            "WAKENSCDP01",
            "WAKENSCDP02",
            "WAPA2SCDP01",
            "WAPASSCDP01",
            "WAPT2SCDP01",
            "WAPTNSCDP01",
            "WAQUISCDP01",
            "WARICSCDP01",
            "WAWARSCDP01"});
            this.server3.Location = new System.Drawing.Point(145, 88);
            this.server3.Name = "server3";
            this.server3.Size = new System.Drawing.Size(121, 21);
            this.server3.Sorted = true;
            this.server3.TabIndex = 24;
            this.server3.SelectedIndexChanged += new System.EventHandler(this.server3_SelectedIndexChanged);
            // 
            // jobstatus10
            // 
            this.jobstatus10.AutoSize = true;
            this.jobstatus10.Location = new System.Drawing.Point(504, 337);
            this.jobstatus10.Name = "jobstatus10";
            this.jobstatus10.Size = new System.Drawing.Size(72, 13);
            this.jobstatus10.TabIndex = 44;
            this.jobstatus10.Text = "Job 10 Status";
            this.jobstatus10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.jobstatus10.Visible = false;
            // 
            // jobstatus3
            // 
            this.jobstatus3.AutoSize = true;
            this.jobstatus3.Location = new System.Drawing.Point(504, 92);
            this.jobstatus3.Name = "jobstatus3";
            this.jobstatus3.Size = new System.Drawing.Size(66, 13);
            this.jobstatus3.TabIndex = 37;
            this.jobstatus3.Text = "Job 3 Status";
            this.jobstatus3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.jobstatus3.Visible = false;
            // 
            // new8
            // 
            this.new8.Enabled = false;
            this.new8.Location = new System.Drawing.Point(398, 264);
            this.new8.Name = "new8";
            this.new8.Size = new System.Drawing.Size(100, 20);
            this.new8.TabIndex = 13;
            this.new8.TextChanged += new System.EventHandler(this.new8_TextChanged);
            // 
            // old10
            // 
            this.old10.Enabled = false;
            this.old10.Location = new System.Drawing.Point(280, 334);
            this.old10.Name = "old10";
            this.old10.Size = new System.Drawing.Size(100, 20);
            this.old10.TabIndex = 10;
            // 
            // jobstatus9
            // 
            this.jobstatus9.AutoSize = true;
            this.jobstatus9.Location = new System.Drawing.Point(504, 302);
            this.jobstatus9.Name = "jobstatus9";
            this.jobstatus9.Size = new System.Drawing.Size(66, 13);
            this.jobstatus9.TabIndex = 43;
            this.jobstatus9.Text = "Job 9 Status";
            this.jobstatus9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.jobstatus9.Visible = false;
            // 
            // jobstatus4
            // 
            this.jobstatus4.AutoSize = true;
            this.jobstatus4.Location = new System.Drawing.Point(504, 127);
            this.jobstatus4.Name = "jobstatus4";
            this.jobstatus4.Size = new System.Drawing.Size(66, 13);
            this.jobstatus4.TabIndex = 38;
            this.jobstatus4.Text = "Job 4 Status";
            this.jobstatus4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.jobstatus4.Visible = false;
            // 
            // jobstatus8
            // 
            this.jobstatus8.AutoSize = true;
            this.jobstatus8.Location = new System.Drawing.Point(504, 267);
            this.jobstatus8.Name = "jobstatus8";
            this.jobstatus8.Size = new System.Drawing.Size(66, 13);
            this.jobstatus8.TabIndex = 42;
            this.jobstatus8.Text = "Job 8 Status";
            this.jobstatus8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.jobstatus8.Visible = false;
            // 
            // server2
            // 
            this.server2.Enabled = false;
            this.server2.FormattingEnabled = true;
            this.server2.Items.AddRange(new object[] {
            "ABTABSCDP01",
            "IDAMESCDP01",
            "IDEAGSCDP01",
            "IDTWISCDP01",
            "ILNAPSCDP01",
            "LADELSCDP01",
            "ORBO3SCDP01",
            "ORBOASCDP01",
            "WACONSCDP01",
            "WAKENSCCM01",
            "WAKENSCDP01",
            "WAKENSCDP02",
            "WAPA2SCDP01",
            "WAPASSCDP01",
            "WAPT2SCDP01",
            "WAPTNSCDP01",
            "WAQUISCDP01",
            "WARICSCDP01",
            "WAWARSCDP01"});
            this.server2.Location = new System.Drawing.Point(145, 53);
            this.server2.Name = "server2";
            this.server2.Size = new System.Drawing.Size(121, 21);
            this.server2.Sorted = true;
            this.server2.TabIndex = 23;
            this.server2.SelectedIndexChanged += new System.EventHandler(this.server2_SelectedIndexChanged);
            // 
            // new9
            // 
            this.new9.Enabled = false;
            this.new9.Location = new System.Drawing.Point(398, 299);
            this.new9.Name = "new9";
            this.new9.Size = new System.Drawing.Size(100, 20);
            this.new9.TabIndex = 12;
            this.new9.TextChanged += new System.EventHandler(this.new9_TextChanged);
            // 
            // jobstatus5
            // 
            this.jobstatus5.AutoSize = true;
            this.jobstatus5.Location = new System.Drawing.Point(504, 162);
            this.jobstatus5.Name = "jobstatus5";
            this.jobstatus5.Size = new System.Drawing.Size(66, 13);
            this.jobstatus5.TabIndex = 39;
            this.jobstatus5.Text = "Job 5 Status";
            this.jobstatus5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.jobstatus5.Visible = false;
            // 
            // jobstatus7
            // 
            this.jobstatus7.AutoSize = true;
            this.jobstatus7.Location = new System.Drawing.Point(504, 232);
            this.jobstatus7.Name = "jobstatus7";
            this.jobstatus7.Size = new System.Drawing.Size(66, 13);
            this.jobstatus7.TabIndex = 41;
            this.jobstatus7.Text = "Job 7 Status";
            this.jobstatus7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.jobstatus7.Visible = false;
            // 
            // new10
            // 
            this.new10.Enabled = false;
            this.new10.Location = new System.Drawing.Point(398, 334);
            this.new10.Name = "new10";
            this.new10.Size = new System.Drawing.Size(100, 20);
            this.new10.TabIndex = 11;
            this.new10.TextChanged += new System.EventHandler(this.new10_TextChanged);
            // 
            // server01
            // 
            this.server01.Enabled = false;
            this.server01.FormattingEnabled = true;
            this.server01.Items.AddRange(new object[] {
            "ABTABSCDP01",
            "IDAMESCDP01",
            "IDEAGSCDP01",
            "IDTWISCDP01",
            "ILNAPSCDP01",
            "LADELSCDP01",
            "ORBO3SCDP01",
            "ORBOASCDP01",
            "WACONSCDP01",
            "WAKENSCCM01",
            "WAKENSCDP01",
            "WAKENSCDP02",
            "WAPA2SCDP01",
            "WAPASSCDP01",
            "WAPT2SCDP01",
            "WAPTNSCDP01",
            "WAQUISCDP01",
            "WARICSCDP01",
            "WAWARSCDP01"});
            this.server01.Location = new System.Drawing.Point(145, 18);
            this.server01.Name = "server01";
            this.server01.Size = new System.Drawing.Size(121, 21);
            this.server01.Sorted = true;
            this.server01.TabIndex = 22;
            this.server01.SelectedIndexChanged += new System.EventHandler(this.server1_SelectedIndexChanged);
            // 
            // jobstatus6
            // 
            this.jobstatus6.AutoSize = true;
            this.jobstatus6.Location = new System.Drawing.Point(504, 197);
            this.jobstatus6.Name = "jobstatus6";
            this.jobstatus6.Size = new System.Drawing.Size(66, 13);
            this.jobstatus6.TabIndex = 40;
            this.jobstatus6.Text = "Job 6 Status";
            this.jobstatus6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.jobstatus6.Visible = false;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.ping_old_host_button);
            this.groupBox7.Controls.Add(this.copy_job_button);
            this.groupBox7.Controls.Add(this.clear_job_button);
            this.groupBox7.Controls.Add(this.clearserversbutton);
            this.groupBox7.Controls.Add(this.copyserversbutton);
            this.groupBox7.Controls.Add(this.start_migration_button);
            this.groupBox7.Controls.Add(this.paste_from_excel_button);
            this.groupBox7.Location = new System.Drawing.Point(7, 7);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(592, 69);
            this.groupBox7.TabIndex = 0;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Migration Job Tasks";
            // 
            // ping_old_host_button
            // 
            this.ping_old_host_button.FlatAppearance.BorderSize = 0;
            this.ping_old_host_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ping_old_host_button.ImageIndex = 10;
            this.ping_old_host_button.ImageList = this.imgButton;
            this.ping_old_host_button.Location = new System.Drawing.Point(466, 30);
            this.ping_old_host_button.Name = "ping_old_host_button";
            this.ping_old_host_button.Size = new System.Drawing.Size(32, 32);
            this.ping_old_host_button.TabIndex = 57;
            this.ping.SetToolTip(this.ping_old_host_button, "Click here to ping these systems to \r\nverify they are online.");
            this.ping_old_host_button.UseVisualStyleBackColor = true;
            this.ping_old_host_button.Click += new System.EventHandler(this.ping_old_host_button_Click);
            // 
            // copy_job_button
            // 
            this.copy_job_button.FlatAppearance.BorderSize = 0;
            this.copy_job_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.copy_job_button.ImageIndex = 0;
            this.copy_job_button.ImageList = this.imgButton;
            this.copy_job_button.Location = new System.Drawing.Point(19, 25);
            this.copy_job_button.Name = "copy_job_button";
            this.copy_job_button.Size = new System.Drawing.Size(32, 32);
            this.copy_job_button.TabIndex = 55;
            this.copy_job_button.TabStop = false;
            this.copy_job_button.Text = "button8";
            this.copy_server_tooltip.SetToolTip(this.copy_job_button, "Copy Servers\r\n\r\nClick here to copy the server to all jobs.");
            this.copy_job_button.UseVisualStyleBackColor = true;
            this.copy_job_button.Click += new System.EventHandler(this.copy_job_button_Click);
            // 
            // clear_job_button
            // 
            this.clear_job_button.FlatAppearance.BorderSize = 0;
            this.clear_job_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.clear_job_button.ImageKey = "edit-clear.png";
            this.clear_job_button.ImageList = this.imgButton;
            this.clear_job_button.Location = new System.Drawing.Point(57, 25);
            this.clear_job_button.Name = "clear_job_button";
            this.clear_job_button.Size = new System.Drawing.Size(32, 32);
            this.clear_job_button.TabIndex = 56;
            this.clear_job_button.TabStop = false;
            this.clear_servers.SetToolTip(this.clear_job_button, "Clear Servers\r\n\r\nClick here to clear all the servers selected.");
            this.clear_job_button.UseVisualStyleBackColor = true;
            this.clear_job_button.Click += new System.EventHandler(this.clear_job_button_Click);
            // 
            // clearserversbutton
            // 
            this.clearserversbutton.FlatAppearance.BorderSize = 0;
            this.clearserversbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.clearserversbutton.ImageKey = "edit-clear.png";
            this.clearserversbutton.ImageList = this.imgButton;
            this.clearserversbutton.Location = new System.Drawing.Point(186, 30);
            this.clearserversbutton.Name = "clearserversbutton";
            this.clearserversbutton.Size = new System.Drawing.Size(32, 32);
            this.clearserversbutton.TabIndex = 33;
            this.clearserversbutton.TabStop = false;
            this.clear_servers.SetToolTip(this.clearserversbutton, "Clear Servers\r\n\r\nClick here to clear all the servers selected.");
            this.clearserversbutton.UseVisualStyleBackColor = true;
            this.clearserversbutton.Click += new System.EventHandler(this.clearserversbutton_Click);
            // 
            // copyserversbutton
            // 
            this.copyserversbutton.FlatAppearance.BorderSize = 0;
            this.copyserversbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.copyserversbutton.ImageIndex = 0;
            this.copyserversbutton.ImageList = this.imgButton;
            this.copyserversbutton.Location = new System.Drawing.Point(148, 30);
            this.copyserversbutton.Name = "copyserversbutton";
            this.copyserversbutton.Size = new System.Drawing.Size(32, 32);
            this.copyserversbutton.TabIndex = 32;
            this.copyserversbutton.TabStop = false;
            this.copyserversbutton.Text = "button8";
            this.copy_server_tooltip.SetToolTip(this.copyserversbutton, "Copy Servers\r\n\r\nClick here to copy the server to all jobs.");
            this.copyserversbutton.UseVisualStyleBackColor = true;
            this.copyserversbutton.Click += new System.EventHandler(this.copyserversbutton_Click);
            // 
            // start_migration_button
            // 
            this.start_migration_button.FlatAppearance.BorderSize = 0;
            this.start_migration_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.start_migration_button.ImageIndex = 7;
            this.start_migration_button.ImageList = this.imgButton;
            this.start_migration_button.Location = new System.Drawing.Point(554, 30);
            this.start_migration_button.Name = "start_migration_button";
            this.start_migration_button.Size = new System.Drawing.Size(32, 32);
            this.start_migration_button.TabIndex = 34;
            this.start_migration_button.TabStop = false;
            this.start_batch.SetToolTip(this.start_migration_button, "\r\nThis starts the batch migration processes.");
            this.start_migration_button.UseVisualStyleBackColor = true;
            this.start_migration_button.Click += new System.EventHandler(this.button8_Click);
            // 
            // paste_from_excel_button
            // 
            this.paste_from_excel_button.FlatAppearance.BorderSize = 0;
            this.paste_from_excel_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.paste_from_excel_button.ImageIndex = 8;
            this.paste_from_excel_button.ImageList = this.imgButton;
            this.paste_from_excel_button.Location = new System.Drawing.Point(280, 30);
            this.paste_from_excel_button.Name = "paste_from_excel_button";
            this.paste_from_excel_button.Size = new System.Drawing.Size(32, 32);
            this.paste_from_excel_button.TabIndex = 21;
            this.paste_from_excel_button.TabStop = false;
            this.copy_from_excel_tooltip.SetToolTip(this.paste_from_excel_button, resources.GetString("paste_from_excel_button.ToolTip"));
            this.paste_from_excel_button.UseVisualStyleBackColor = true;
            this.paste_from_excel_button.Click += new System.EventHandler(this.button7_Click);
            // 
            // homedrive
            // 
            this.homedrive.BackColor = System.Drawing.SystemColors.Control;
            this.homedrive.Controls.Add(this.groupBox10);
            this.homedrive.Controls.Add(this.groupBox8);
            this.homedrive.Cursor = System.Windows.Forms.Cursors.Hand;
            this.homedrive.Location = new System.Drawing.Point(4, 22);
            this.homedrive.Name = "homedrive";
            this.homedrive.Padding = new System.Windows.Forms.Padding(3);
            this.homedrive.Size = new System.Drawing.Size(609, 622);
            this.homedrive.TabIndex = 4;
            this.homedrive.Text = "Home Drive";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.home_log);
            this.groupBox10.Location = new System.Drawing.Point(8, 452);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(592, 162);
            this.groupBox10.TabIndex = 6;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Log";
            // 
            // home_log
            // 
            this.home_log.Location = new System.Drawing.Point(6, 19);
            this.home_log.Name = "home_log";
            this.home_log.Size = new System.Drawing.Size(580, 130);
            this.home_log.TabIndex = 3;
            this.home_log.TabStop = false;
            this.home_log.Text = "";
            this.home_log.WordWrap = false;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.lookup_home_drive_button);
            this.groupBox8.Controls.Add(this.break_users);
            this.groupBox8.Controls.Add(this.button7);
            this.groupBox8.Controls.Add(this.import_home_drive_users_button);
            this.groupBox8.Controls.Add(this.label13);
            this.groupBox8.Location = new System.Drawing.Point(8, 6);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(592, 440);
            this.groupBox8.TabIndex = 5;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Users";
            // 
            // lookup_home_drive_button
            // 
            this.lookup_home_drive_button.FlatAppearance.BorderSize = 0;
            this.lookup_home_drive_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lookup_home_drive_button.ImageIndex = 12;
            this.lookup_home_drive_button.ImageList = this.imgButton;
            this.lookup_home_drive_button.Location = new System.Drawing.Point(179, 110);
            this.lookup_home_drive_button.Name = "lookup_home_drive_button";
            this.lookup_home_drive_button.Size = new System.Drawing.Size(48, 45);
            this.lookup_home_drive_button.TabIndex = 5;
            this.lookup_home_drive_button.UseVisualStyleBackColor = true;
            this.lookup_home_drive_button.Click += new System.EventHandler(this.lookup_home_drive_button_Click);
            // 
            // break_users
            // 
            this.break_users.Location = new System.Drawing.Point(60, 98);
            this.break_users.Multiline = true;
            this.break_users.Name = "break_users";
            this.break_users.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.break_users.Size = new System.Drawing.Size(113, 293);
            this.break_users.TabIndex = 0;
            this.break_users.TextChanged += new System.EventHandler(this.break_users_TextChanged);
            // 
            // button7
            // 
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.ImageIndex = 0;
            this.button7.ImageList = this.largebuttons;
            this.button7.Location = new System.Drawing.Point(308, 110);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(256, 256);
            this.button7.TabIndex = 4;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click_1);
            // 
            // largebuttons
            // 
            this.largebuttons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("largebuttons.ImageStream")));
            this.largebuttons.TransparentColor = System.Drawing.Color.Transparent;
            this.largebuttons.Images.SetKeyName(0, "damage_city.png");
            // 
            // import_home_drive_users_button
            // 
            this.import_home_drive_users_button.FlatAppearance.BorderSize = 0;
            this.import_home_drive_users_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.import_home_drive_users_button.ImageIndex = 11;
            this.import_home_drive_users_button.ImageList = this.imgButton;
            this.import_home_drive_users_button.Location = new System.Drawing.Point(166, 49);
            this.import_home_drive_users_button.Name = "import_home_drive_users_button";
            this.import_home_drive_users_button.Size = new System.Drawing.Size(32, 32);
            this.import_home_drive_users_button.TabIndex = 1;
            this.import_home_drive_users_button.UseVisualStyleBackColor = true;
            this.import_home_drive_users_button.Click += new System.EventHandler(this.import_home_drive_users_button_Click);
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(47, 44);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(113, 37);
            this.label13.TabIndex = 2;
            this.label13.Text = "Paste username or Browse to import";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // username1
            // 
            this.username1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.username1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.username1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
            this.username1.Location = new System.Drawing.Point(103, 4);
            this.username1.Name = "username1";
            this.username1.Size = new System.Drawing.Size(94, 20);
            this.username1.TabIndex = 10;
            this.username1.Text = "lw\\";
            // 
            // password1
            // 
            this.password1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.password1.Location = new System.Drawing.Point(103, 34);
            this.password1.Name = "password1";
            this.password1.Size = new System.Drawing.Size(94, 20);
            this.password1.TabIndex = 12;
            this.password1.UseSystemPasswordChar = true;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label5.Location = new System.Drawing.Point(41, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Password:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label6.Location = new System.Drawing.Point(39, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Username:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // printDialog1
            // 
            this.printDialog1.AllowSelection = true;
            this.printDialog1.UseEXDialog = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // load_migration_list
            // 
            this.load_migration_list.Filter = "\"CSV Files\"|*.csv|\"Excel Files|*.xls*";
            this.load_migration_list.InitialDirectory = "%userprofile%";
            // 
            // process1
            // 
            this.process1.StartInfo.Domain = "";
            this.process1.StartInfo.LoadUserProfile = false;
            this.process1.StartInfo.Password = null;
            this.process1.StartInfo.StandardErrorEncoding = null;
            this.process1.StartInfo.StandardOutputEncoding = null;
            this.process1.StartInfo.UserName = "";
            this.process1.SynchronizingObject = this;
            // 
            // copy_from_excel_tooltip
            // 
            this.copy_from_excel_tooltip.IsBalloon = true;
            this.copy_from_excel_tooltip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.copy_from_excel_tooltip.ToolTipTitle = "Copy From Excel";
            // 
            // copy_server_tooltip
            // 
            this.copy_server_tooltip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.copy_server_tooltip.ToolTipTitle = "Copy Servers Down";
            // 
            // clear_servers
            // 
            this.clear_servers.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.clear_servers.ToolTipTitle = "Clear All Servers";
            // 
            // start_batch
            // 
            this.start_batch.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.start_batch.ToolTipTitle = "Start Batch Migrations";
            // 
            // ping
            // 
            this.ping.ToolTipTitle = "Ping Systems";
            // 
            // Win10Utils
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 679);
            this.Controls.Add(this.old_system_status);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.tabs);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Win10Utils";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Windows 10 Utilities";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WinUtils_FormClosing);
            this.Load += new System.EventHandler(this.Win10Utils_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tabs.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.desc_group.ResumeLayout(false);
            this.desc_group.PerformLayout();
            this.logbox.ResumeLayout(false);
            this.logbox.PerformLayout();
            this.adminCreds.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.migration.ResumeLayout(false);
            this.migration.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.server_jobs_groupbox.ResumeLayout(false);
            this.server_jobs_groupbox.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.homedrive.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fIleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem; 
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetFormToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Label old_computer_label;
        private System.Windows.Forms.TextBox new_computer;
        private System.Windows.Forms.Label new_computer_label;
        private System.Windows.Forms.Button lookup_groups_button;
        private System.Windows.Forms.ListBox groups_listbox;
        private System.Windows.Forms.Label old_system_status;
        private System.Windows.Forms.Label system_status;
        public System.Windows.Forms.TextBox old_computer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox password;
        private System.DirectoryServices.DirectorySearcher directorySearcher;
        private System.Windows.Forms.Button sync_groups;
        private System.Windows.Forms.ListBox new_computer_groups;
        private System.Windows.Forms.Label new_system_status;
        private System.DirectoryServices.DirectoryEntry directoryEntry;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox adminCreds;
        private System.Windows.Forms.GroupBox logbox;
        private System.Windows.Forms.TextBox log;
        private System.Windows.Forms.Button remove_right;
        private System.Windows.Forms.GroupBox desc_group;
        private System.Windows.Forms.Button set_desc;
        private System.Windows.Forms.TextBox computer_description;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox new_desc;
        private System.Windows.Forms.Button copy_desc;
        private System.Windows.Forms.ImageList imgButton;
        private System.Windows.Forms.PrintDialog printDialog1;
        
        
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox user_log;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button remove_from_left_button;
        private System.Windows.Forms.Button remove_from_right_button;
        private System.Windows.Forms.ListBox copy_user_groups;
        private System.Windows.Forms.ListBox add_user_groups;
        private System.Windows.Forms.Button add_user_groups_button;
        private System.Windows.Forms.TextBox username1;
        private System.Windows.Forms.TextBox password1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox user2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox User1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem computerGroupsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem userGroupsToolStripMenuItem1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox computer_list;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ToolStripMenuItem pingReportToolStripMenuItem1;
        private System.Windows.Forms.CheckBox include_groups_checkbox;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TabPage migration;
        private System.Windows.Forms.TextBox old10;
        private System.Windows.Forms.TextBox old9;
        private System.Windows.Forms.TextBox old8;
        private System.Windows.Forms.TextBox old7;
        private System.Windows.Forms.TextBox old6;
        private System.Windows.Forms.TextBox old5;
        private System.Windows.Forms.TextBox old4;
        private System.Windows.Forms.TextBox old3;
        private System.Windows.Forms.TextBox old2;
        private System.Windows.Forms.TextBox old01;
        private System.Windows.Forms.TextBox new01;
        private System.Windows.Forms.TextBox new2;
        private System.Windows.Forms.TextBox new3;
        private System.Windows.Forms.TextBox new4;
        private System.Windows.Forms.TextBox new5;
        private System.Windows.Forms.TextBox new6;
        private System.Windows.Forms.TextBox new7;
        private System.Windows.Forms.TextBox new8;
        private System.Windows.Forms.TextBox new9;
        private System.Windows.Forms.TextBox new10;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button paste_from_excel_button;
        private System.Windows.Forms.OpenFileDialog load_migration_list;
        private System.Windows.Forms.ToolStripMenuItem migration_tooltip;
        private System.Windows.Forms.ComboBox server10;
        private System.Windows.Forms.ComboBox server9;
        private System.Windows.Forms.ComboBox server8;
        private System.Windows.Forms.ComboBox server7;
        private System.Windows.Forms.ComboBox server6;
        private System.Windows.Forms.ComboBox server5;
        private System.Windows.Forms.ComboBox server4;
        private System.Windows.Forms.ComboBox server3;
        private System.Windows.Forms.ComboBox server2;
        private System.Windows.Forms.ComboBox server01;
        private System.Windows.Forms.Button copyserversbutton;
        private System.Windows.Forms.Button clearserversbutton;
        private System.Diagnostics.Process process1;
        private System.Windows.Forms.Button start_migration_button;
        private System.Windows.Forms.ToolTip copy_from_excel_tooltip;
        private System.Windows.Forms.ToolTip copy_server_tooltip;
        private System.Windows.Forms.ToolTip clear_servers;
        private System.Windows.Forms.ToolTip start_batch;
        private System.Windows.Forms.Label jobstatus10;
        private System.Windows.Forms.Label jobstatus9;
        private System.Windows.Forms.Label jobstatus8;
        private System.Windows.Forms.Label jobstatus7;
        private System.Windows.Forms.Label jobstatus6;
        private System.Windows.Forms.Label jobstatus5;
        private System.Windows.Forms.Label jobstatus4;
        private System.Windows.Forms.Label jobstatus3;
        private System.Windows.Forms.Label jobstatus2;
        private System.Windows.Forms.Label jobstatus01;
        private System.Windows.Forms.ComboBox migration_job10;
        private System.Windows.Forms.ComboBox migration_job6;
        private System.Windows.Forms.ComboBox migration_job9;
        private System.Windows.Forms.ComboBox migration_job8;
        private System.Windows.Forms.ComboBox migration_job7;
        private System.Windows.Forms.ComboBox migration_job5;
        private System.Windows.Forms.ComboBox migration_job4;
        private System.Windows.Forms.ComboBox migration_job3;
        private System.Windows.Forms.ComboBox migration_job2;
        private System.Windows.Forms.ComboBox migration_job01;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox server_jobs_groupbox;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button copy_job_button;
        private System.Windows.Forms.Button clear_job_button;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox migration_log;
        private System.Windows.Forms.RichTextBox ping_report;
        private System.Windows.Forms.Button sync_all_groups_button;
        private System.Windows.Forms.Button ping_old_host_button;
        private System.Windows.Forms.ToolTip ping;
        private System.Windows.Forms.ToolStripMenuItem killHomeDriveToolStripMenuItem;
        private System.Windows.Forms.TabPage homedrive;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button import_home_drive_users_button;
        private System.Windows.Forms.TextBox break_users;
        private System.Windows.Forms.RichTextBox home_log;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.ImageList largebuttons;
        private System.Windows.Forms.Button lookup_home_drive_button;
        private System.Windows.Forms.Button move_ou_button;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button mass_move_ou_button;
        private System.Windows.Forms.RichTextBox ou_log;
        private System.Windows.Forms.TextBox ou_to_move_list;
    }
}

